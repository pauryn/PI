import { trigger, transition, style, animate } from '@angular/animations';
import { formatDate } from '@angular/common';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { DomSanitizer, SafeResourceUrl } from '@angular/platform-browser';
import { DzialyDropdown } from '../dzialyDropdown';
import { SprzetService } from '../service/sprzet.service';
import { Sprzet } from '../Sprzet';

@Component({
  selector: 'app-sprzet',
  templateUrl: './sprzet.component.html',
  styleUrls: ['./sprzet.component.scss'],
  animations: [
    trigger('fadeInOut', [
      transition(':enter', [   
        style({opacity:0}),
        animate(300, style({opacity:1})) 
      ]),
      transition(':leave', [   
        animate(300, style({opacity:0})) 
      ])
    ])
  ]
})
export class SprzetComponent implements OnInit {

  sprzetForm: FormGroup;
  tableSize = '80vh';
  sprzet: Array<Sprzet> = [];
  dzialyDropdown: Array<DzialyDropdown> = [];
  deactivate: boolean = false;
  toDeactivate : number;
  deactivateInfo: string;
  deactivated: boolean = false;
  photo: boolean = false;
  photoSrc: SafeResourceUrl;
  addSprzet: boolean = false;
  editSprzet: boolean = false;
  editedSprzet: number;
  editedPhoto: any;

  constructor(
    private sprzetService: SprzetService,
    private sanitizer: DomSanitizer,
    private formBuilder: FormBuilder
  ) {
    this.sprzetForm = this.formBuilder.group({
      dz: ['', Validators.required],
      zdj: '',
      nazwa: ['', Validators.required],
      wpr: ['', Validators.required],
      wyc: null
    })
   }

  ngOnInit(): void {
    this.getAllSprzet();
  }

  getAllSprzet() : void {
    this.sprzet = [];
    this.sprzetService.getAllSprzet().subscribe(res => {
      for(let i=0; i<Object.keys(res).length; i++) {
        let binary = '';
        let bytes = new Uint8Array(res[i].SP_Zdjecie.data);
        let len = bytes.byteLength;
        for (let j = 0; j < len; j++) 
          binary += String.fromCharCode( bytes[j] );
        
        res[i].SP_Zdjecie = btoa(binary);
        this.sprzet.push(new Sprzet(res[i].SP_Id,res[i].SP_Nazwa, res[i].DZ_Nazwa, res[i].SP_Data_Wpr, res[i].SP_Data_Wyc, res[i].SP_Zdjecie, true));
      }
    })
  }

  deactivatePopUp(id:number): void{
    this.deactivate = true;
    this.toDeactivate = id;

  }

  hideDeactivate(): void{
    this.deactivate = false;
    this.deactivated = false;
    this.toDeactivate = 0;
  }

  submitDeactivate(): void{
    this.sprzetService.deactivateSprzet(this.toDeactivate).subscribe(res => {
          this.deactivateInfo='Poprawnie usunięto dane.';
          this.getAllSprzet();
        }, (err) => {
          this.deactivateInfo='Wystąpił błąd';
        });
    this.deactivate=false;
    this.deactivated=true;
  }

  openPhoto(photo: string): void {
    this.photoSrc=this.sanitizer.bypassSecurityTrustResourceUrl('data:image/jpg;base64,'+ photo); 
    this.photo = true;
  }

  closePhoto(): void {
    this.photoSrc='';
    this.photo=false;
  }

  showAddSprzet(): void {
    this.dzialyDropdown=[];
    this.sprzetForm.controls.dz.setValue(null);
    this.sprzetForm.controls.nazwa.setValue(null);
    this.sprzetForm.controls.zdj.setValue(null);
    this.sprzetForm.controls.wpr.setValue(null);
    this.sprzetForm.controls.wyc.setValue(null);
    this.sprzetService.getDzialyDropdown().subscribe(res=> {
      for(let i=0; i<Object.keys(res).length; i++){
        
        this.dzialyDropdown.push(new DzialyDropdown(res[i].DZ_Id, res[i].DZ_Nazwa));
      }
    });
    this.addSprzet= true;
  }

  submitAddSprzet():void {
    let binary = '';
    let bytes = new Uint8Array(this.sprzetForm.value.zdj);
    let len = bytes.byteLength;
    for (let j = 0; j < len; j++) 
      binary += String.fromCharCode( bytes[j] );
    this.sprzetForm.value.zdj = btoa(binary);
    console.log(this.sprzetForm.value.zdj);
    this.sprzetService.addSprzet(this.sprzetForm.value).subscribe(() => {
      this.deactivateInfo='Poprawnie dodano Sprzęt';
      this.getAllSprzet();
       
    }, (err) => {
        this.deactivateInfo='Wystąpił błąd';
    });
    this.addSprzet=false;
    this.deactivated=true;
  }

  closeAddSprzet():void {
    this.addSprzet=false;
  }

  showEditSprzet(id: number, photo: string): void {
    this.dzialyDropdown=[];
    this.editedSprzet=id;
    this.editedPhoto = photo;
    this.sprzetService.getDzialyDropdown().subscribe(res=> {
      for(let i=0; i<Object.keys(res).length; i++){
        
        this.dzialyDropdown.push(new DzialyDropdown(res[i].DZ_Id, res[i].DZ_Nazwa));
      }
    });
    this.sprzetService.getOneSprzet(id).subscribe(res=>{
      this.sprzetForm.controls.dz.setValue(res[0].DZ_Id);
      this.sprzetForm.controls.nazwa.setValue(res[0].SP_Nazwa);
      this.sprzetForm.controls.wpr.setValue(formatDate(res[0].SP_Data_Wpr, "yyyy-MM-dd", 'pl-PL'));
      if(res[0].SP_Data_Wyc != null)this.sprzetForm.controls.wyc.setValue(formatDate(res[0].SP_Data_Wyc, "yyyy-MM-dd", 'pl-PL'));
     })
    this.editSprzet= true;
  }
  submitEditSprzet():void {
    if(this.sprzetForm.value.zdj != '') {
      let binary = '';
      let bytes = new Uint8Array(this.sprzetForm.value.zdj);
      let len = bytes.byteLength;
      for (let j = 0; j < len; j++) 
        binary += String.fromCharCode( bytes[j] );
      this.sprzetForm.value.zdj = btoa(binary);
    }
    this.sprzetService.editSprzet(this.sprzetForm.value, this.editedSprzet).subscribe(() => {
      this.deactivateInfo='Poprawnie edytowano Sprzęt';
      this.getAllSprzet();
      
      }, (err) => {
        this.deactivateInfo='Wystąpił błąd';
        console.log(err);
    });
    this.editSprzet=false;
    this.deactivated=true;
  }

  closeEditSprzet():void {
    this.editSprzet=false;
  }

  searchSprzet(event: any){
    if (event.target.value) {
      let text = event.target.value.toLowerCase();
      let filtered = this.sprzet.map(i => { return i.nazwa + ' ' + i.dzial + ' ' + formatDate(i.dataWpr, "dd.MM.yyyy", 'pl-PL') + ' ' + formatDate(i.dataWyc, "dd.MM.yyyy", 'pl-PL')});
      filtered.filter((i, index) => {
        if(i.toLowerCase().includes(text)) 
          this.sprzet[index].display = true;
        else
          this.sprzet[index].display = false;
      });
    }
    else
      this.sprzet.forEach( i => i.display = true);
  }

  onFileChange(event) {    
    if (event.target.files.length > 0) {
      const file = event.target.files[0];
      const reader = new FileReader();
      
      reader.onload = () => { 
        this.sprzetForm.value.zdj = reader.result;
      }
      reader.readAsArrayBuffer(file);
     
    }
  }

}
