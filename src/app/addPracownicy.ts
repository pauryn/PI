export class addPracownicy{
    constructor(
        public dz: number,
        public imie: string,
        public nazw: string,
        public ur: Date,
        public adres: string,
        public zatr: Date,
        public zwol: Date,
        public pesel: number,
        public tel: number,
        public zdj: any
    ){}
}