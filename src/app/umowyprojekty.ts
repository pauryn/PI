export class UmowyProjekty{
    constructor(
        public id: number,
        public firma: string,
        public dataZaw: Date,
        public dataZak: Date,
        public cena: number,
        public zdjecie: string,
        public display: boolean
    ){}
}