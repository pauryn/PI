export class Dzialy{
    constructor(
        public id: number,
        public nazwa: string,
        public kierownik:string,
        public display: boolean
    ){}
}