import { animate, style, transition, trigger } from '@angular/animations';
import { formatDate } from '@angular/common';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { DomSanitizer, SafeResourceUrl } from '@angular/platform-browser';
import { klienci } from '../klienci';
import { KlienciDropdown } from '../klienciDropdown';
import { KlienciService } from '../service/klienci.service';
import { Umowy } from '../umowy';

@Component({
  selector: 'app-klienci',
  templateUrl: './klienci.component.html',
  styleUrls: ['./klienci.component.scss'],
  animations: [
    trigger('fadeInOut', [
      transition(':enter', [   
        style({opacity:0}),
        animate(300, style({opacity:1})) 
      ]),
      transition(':leave', [   
        animate(300, style({opacity:0})) 
      ])
    ])
  ]
})
export class KlienciComponent implements OnInit {

  umowyForm: FormGroup;
  klienciForm: FormGroup;
  klienci: Array<klienci> = [];
  umowy: Array<Umowy> = [];
  klienciDropdown: Array<KlienciDropdown> = [];
  selectedKlienci: number;
  selectedKlienciName: string;
  isHidden: boolean = false;
  tableSize = '80vh';
  deactivate: boolean = false;
  toDeactivate : number;
  deactivateFunction: string;
  deactivateInfo: string;
  deactivated: boolean = false;
  photo: boolean = false;
  photoSrc:SafeResourceUrl;
  addUmowy: boolean = false;
  editUmowy: boolean = false;
  editedUmowy:number;
  addKlienci: boolean = false;
  EditKlienci: boolean = false;
  editedKlienci: number;
  editedPhoto: any;

  constructor(
    private klienciService: KlienciService,
    private sanitizer: DomSanitizer,
    private formBuilder: FormBuilder
  ) {
    this.umowyForm = this.formBuilder.group({
      kl: ['', Validators.required],
      zdj: '',
      cena: ['', Validators.required],
      zaw: ['', Validators.required],
      zak: ['', Validators.required],
    })
    this.klienciForm = this.formBuilder.group({
      firma: ['', Validators.required],
      nip: ['', Validators.required],
      adres: ['', Validators.required],
      tel: ['', Validators.required]
    })
   }

  ngOnInit(): void {
    this.getAllKlienci();
  }

  getAllKlienci() : void {
    this.klienci = [];
    this.klienciService.getAllKlienci().subscribe(res => {
      for(let i=0; i<Object.keys(res).length; i++)
        this.klienci.push(new klienci(res[i].KL_Id, res[i].KL_Firma, res[i].KL_NIP, res[i].KL_Adres, res[i].KL_Telefon,true));
      
    })
  }

  selectKlienci(id:number, name: string): void {
    this.isHidden = false;

    this.selectedKlienci=id;
    this.selectedKlienciName=name;
     
    this.tableSize='40vh';
    
    this.getAllUmowy(id);
    
    this.isHidden=true;
  }

  getAllUmowy(id: number) {
    this.umowy=[];
    this.klienciService.getAllUmowy(id).subscribe(res => {
      for(let i=0; i<Object.keys(res).length; i++){
        let binary = '';
        let bytes = new Uint8Array(res[i].UM_Zdjecie.data);
        let len = bytes.byteLength;
        for (let j = 0; j < len; j++) 
          binary += String.fromCharCode( bytes[j] );
        
        res[i].UM_Zdjecie = btoa(binary);

        this.umowy.push(new Umowy(res[i].UM_Id, res[i].KL_ID, res[i].UM_Cena, res[i].UM_Data_zaw, res[i].UM_Data_Zak, res[i].UM_Zdjecie, true));
      }
    });
  }

  deactivatePopUp(id:number, f:string): void{
    this.deactivate = true;
    this.toDeactivate = id;
    this.deactivateFunction = f;

  }

  hideDeactivate(): void{
    this.deactivate = false;
    this.deactivated = false;
    this.toDeactivate = 0;
    this.deactivateFunction = '';
  }

  submitDeactivate(): void{
    if(this.deactivateFunction=='klienci') {
      this.klienciService.deactivateKlienci(this.toDeactivate).subscribe(res => {
          this.deactivateInfo='Poprawnie usunięto dane.';
          this.getAllKlienci();
        }, (err) => {
          this.deactivateInfo='Wystąpił błąd';
        });
    }
    if(this.deactivateFunction=='umowy') {
      this.klienciService.deactivateUmowy(this.toDeactivate).subscribe(res => {
          this.deactivateInfo='Poprawnie usunięto dane.';
          this.getAllUmowy(this.selectedKlienci);
        }, (err) => {
          this.deactivateInfo='Wystąpił błąd';
        });
    }
    this.deactivate=false;
    this.deactivated=true;
  }

  openPhoto(photo: string): void {
    this.photoSrc=this.sanitizer.bypassSecurityTrustResourceUrl('data:image/jpg;base64,'+ photo); 
    this.photo = true;
  }

  closePhoto(): void {
    this.photoSrc='';
    this.photo=false;
  }

  showUmowy(): void {
    this.klienciDropdown=[];
    this.umowyForm.controls.kl.setValue(null);
    this.umowyForm.controls.cena.setValue(null);
    this.umowyForm.controls.zdj.setValue(null);
    this.umowyForm.controls.zaw.setValue(null);
    this.umowyForm.controls.zak.setValue(null);
    this.klienciService.getKlienciDropdown().subscribe(res=> {
      for(let i=0; i<Object.keys(res).length; i++){
        
        this.klienciDropdown.push(new KlienciDropdown(res[i].KL_Id, res[i].klient));
      }
    });
    this.addUmowy= true;
  }

  submitUmowy():void {
    let binary = '';
    let bytes = new Uint8Array(this.umowyForm.value.zdj);
    let len = bytes.byteLength;
    for (let j = 0; j < len; j++) 
      binary += String.fromCharCode( bytes[j] );
    this.umowyForm.value.zdj = btoa(binary);

      this.klienciService.addUmowy(this.umowyForm.value, this.selectedKlienci).subscribe(() => {
        this.deactivateInfo='Poprawnie dodano Umowę';
        this.getAllUmowy(this.selectedKlienci);
        
      }, (err) => {
        this.deactivateInfo='Wystąpił błąd';
    });
    this.addUmowy=false;
    this.deactivated=true;
  }

  closeUmowy():void {
    this.addUmowy=false;
  }

  showEditUmowy(id: number, photo: String): void {
    this.klienciDropdown=[];
    this.editedUmowy=id;
    this.editedPhoto=photo;
    this.klienciService.getKlienciDropdown().subscribe(res=> {
      for(let i=0; i<Object.keys(res).length; i++){
        
        this.klienciDropdown.push(new KlienciDropdown(res[i].KL_Id, res[i].klient));
      }
    });
    this.klienciService.getOneUmowy(id).subscribe(res=>{
      this.umowyForm.controls.kl.setValue(res[0].KL_Id);
      this.umowyForm.controls.cena.setValue(res[0].UM_Cena);
      this.umowyForm.controls.zaw.setValue(formatDate(res[0].UM_Data_zaw, "yyyy-MM-dd", 'pl-PL'));
      this.umowyForm.controls.zak.setValue(formatDate(res[0].UM_Data_zak, "yyyy-MM-dd", 'pl-PL'));
    })
    this.editUmowy= true;
  }
  submitEditUmowy():void {
    if(this.umowyForm.value.zdj != '') {
      let binary = '';
      let bytes = new Uint8Array(this.umowyForm.value.zdj);
      let len = bytes.byteLength;
      for (let j = 0; j < len; j++) 
        binary += String.fromCharCode( bytes[j] );
      this.umowyForm.value.zdj = btoa(binary);
      console.log(this.umowyForm.value.zdj);
    }
    this.klienciService.editUmowy(this.umowyForm.value, this.editedUmowy).subscribe(() => {
      this.deactivateInfo='Poprawnie edytowano Umowę';
      this.getAllUmowy(this.selectedKlienci);
      
      }, (err) => {
        this.deactivateInfo='Wystąpił błąd';
        console.log(err);
    });
    this.editUmowy=false;
    this.deactivated=true;
  }

  closeEditUmowy():void {
    this.editUmowy=false;
  }

  showAddKlienci(){
    this.klienciForm.controls.firma.setValue(null);
    this.klienciForm.controls.nip.setValue(null);
    this.klienciForm.controls.adres.setValue(null);
    this.klienciForm.controls.tel.setValue(null);
    this.addKlienci= true;
  }

  closeAddKlienci():void {
    this.addKlienci=false;
  }

  submitAddKlienci():void {
    
    this.klienciService.addKlienci(this.klienciForm.value).subscribe(() => {
      this.deactivateInfo='Poprawnie dodano nowego Klienta.';
      this.getAllKlienci();
      
    }, (err) => {
      this.deactivateInfo='Wystąpił błąd';
    });
    this.addKlienci=false;
    this.deactivated=true;
  }

  showEditKlienci(id: number): void {
    this.editedKlienci=id;
    this.klienciService.getOneKlienci(id).subscribe( res=> {
      this.klienciForm.controls.firma.setValue(res[0].KL_Firma);
      this.klienciForm.controls.nip.setValue(res[0].KL_NIP);
      this.klienciForm.controls.adres.setValue(res[0].KL_Adres);
      this.klienciForm.controls.tel.setValue(res[0].KL_Telefon);
    })
    this.EditKlienci= true;
  }

  submitEditKlienci():void {
    this.klienciService.editKlienci(this.klienciForm.value, this.editedKlienci).subscribe(() => {
        this.deactivateInfo='Poprawnie edytowano Klienta';
        this.getAllKlienci();
        
      }, (err) => {
        this.deactivateInfo='Wystąpił błąd';
        console.log(err);
    });
    this.EditKlienci=false;
    this.deactivated=true;
  }

  closeEditKlienci():void {
    this.EditKlienci=false;
  }

  searchKlienci(event: any){
    if (event.target.value) {
      let text = event.target.value.toLowerCase();
      let filtered = this.klienci.map(i => { return i.firma + ' ' + i.adres + ' ' + i.nip + ' ' + i.telefon });
      filtered.filter((i, index) => {
        if(i.toLowerCase().includes(text)) 
          this.klienci[index].display = true;
        else
          this.klienci[index].display = false;
      });
    }
    else
      this.klienci.forEach( i => i.display = true);
  }

  searchUmowy(event: any){
    if (event.target.value) {
      let text = event.target.value.toLowerCase();
      let filtered = this.umowy.map(i => { return i.cena + ' ' + formatDate(i.dataZaw, "dd.MM.yyyy", 'pl-PL')});
      filtered.filter((i, index) => {
        if(i.toLowerCase().includes(text)) 
          this.umowy[index].display = true;
        else
          this.umowy[index].display = false;
      });
    }
    else
      this.umowy.forEach( i => i.display = true);
  }

  onFileChange(event) {    
    if (event.target.files.length > 0) {
      const file = event.target.files[0];
      const reader = new FileReader();
      
      reader.onload = () => { 
        this.umowyForm.value.zdj = reader.result;
        console.log(this.umowyForm.value.zdj);
      }
      reader.readAsArrayBuffer(file);
     
    }
  }
}
