export class addKlienci{
    constructor(
        public firma: String,
        public nip: String,
        public adres: String,
        public tel: String
    ){}
}