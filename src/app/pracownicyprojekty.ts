export class PracownicyProjekty{
    constructor(
        public id: number,
        public imie: string,
        public nazwisko: string,
        public dzial: string,
        public telefon: number,
        public zdjecie: string,
        public display: boolean
    ){}
}