import { HttpClient, HttpErrorResponse, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { addDzialy } from '../addDzialy';

@Injectable({
  providedIn: 'root'
})
export class DzialyService {

    // Node/Express API
    REST_API: string = 'http://localhost:8000/api';

    // Http Header
    httpHeaders = new HttpHeaders().set('Content-Type', 'application/json');
   
    constructor(private httpClient: HttpClient) { }
   
    getAllDzialy(){
      return this.httpClient.get(`${this.REST_API}/dzialy/all`);
    }
   
    
    deactivateDzialy(id:number) {
      let API_URL = `${this.REST_API}/dzialy/deactivate/${id}`;
      return this.httpClient.put(API_URL, { headers: this.httpHeaders })
        .pipe(
          catchError(this.handleError)
        )
    }
  
     getPracownicyDropdown() {
       return this.httpClient.get(`${this.REST_API}/projekty/pracownicy`);
     }
     
     addDzialy(data:addDzialy) {
       let API_URL = `${this.REST_API}/dzialy/add`;
         return this.httpClient.post(API_URL, data)
           .pipe(
             catchError(this.handleError)
           )
     }
   
     editDzialy(data:addDzialy, id:number) {
       let API_URL = `${this.REST_API}/dzialy/edit/${id}`;
       return this.httpClient.put(API_URL, data, { headers: this.httpHeaders })
         .pipe(
           catchError(this.handleError)
         )
     }
   
     getOneDzialy(id:number) {
       let API_URL = `${this.REST_API}/dzialy/one/${id}`;
         return this.httpClient.get(API_URL)
           .pipe(
             catchError(this.handleError)
           )
     }
   
     handleError(error: HttpErrorResponse) {
       let errorMessage = '';
       if (error.error instanceof ErrorEvent) {
         // Handle client error
         errorMessage = error.error.message;
       } else {
         // Handle server error
         errorMessage = `Error Code: ${error.status}\nMessage: ${error.message}`;
       }
       console.log(errorMessage);
       return throwError(errorMessage);
     }
  
}
