import { TestBed } from '@angular/core/testing';

import { KlienciService } from './klienci.service';

describe('KlienciService', () => {
  let service: KlienciService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(KlienciService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
