import { TestBed } from '@angular/core/testing';

import { DzialyService } from './dzialy.service';

describe('DzialyService', () => {
  let service: DzialyService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(DzialyService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
