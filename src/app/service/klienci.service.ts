import { HttpClient, HttpErrorResponse, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { throwError } from 'rxjs';
import { catchError, map } from 'rxjs/operators';
import { addKlienci } from '../addKlienci';
import { addUmowy } from '../addUmowy';

@Injectable({
  providedIn: 'root'
})
export class KlienciService {

   // Node/Express API
   REST_API: string = 'http://localhost:8000/api';

   // Http Header
   httpHeaders = new HttpHeaders().set('Content-Type', 'application/json');
 
   constructor(private httpClient: HttpClient) { }
 
   getAllKlienci(){
     return this.httpClient.get(`${this.REST_API}/klienci/all`);
   }
 
 
   getAllUmowy(id:number) {
     let API_URL = `${this.REST_API}/umowy/all/${id}`;
     return this.httpClient.get(API_URL, { headers: this.httpHeaders })
       .pipe(map((res: any) => {
           return res || {}
         }),
         catchError(this.handleError)
       )
   }
  
   deactivateKlienci(id:number) {
     let API_URL = `${this.REST_API}/klienci/deactivate/${id}`;
     return this.httpClient.put(API_URL, { headers: this.httpHeaders })
       .pipe(
         catchError(this.handleError)
       )
   }
 
   deactivateUmowy(id:number) {
     let API_URL = `${this.REST_API}/umowy/deactivate/${id}`;
     return this.httpClient.put(API_URL, { headers: this.httpHeaders })
       .pipe(
         catchError(this.handleError)
       )
   }
 
   getKlienciDropdown() {
     return this.httpClient.get(`${this.REST_API}/umowy/klienci`);
   }
  
   addUmowy(data: addUmowy, id:number){
       let API_URL = `${this.REST_API}/umowy/add/${id}`;
       return this.httpClient.post(API_URL, data)
         .pipe(
           catchError(this.handleError)
         )
   }
 
   getOneUmowy(id: number) {
     let API_URL = `${this.REST_API}/umowy/one/${id}`;
       return this.httpClient.get(API_URL)
         .pipe(
           catchError(this.handleError)
         )
   }
 
   editUmowy(data: addUmowy, id:number) {
     let API_URL = `${this.REST_API}/umowy/edit/${id}`;
     return this.httpClient.put(API_URL, data, { headers: this.httpHeaders })
       .pipe(
         catchError(this.handleError)
       )
   }
 
   addKlienci(data:addKlienci) {
     let API_URL = `${this.REST_API}/klienci/add`;
       return this.httpClient.post(API_URL, data)
         .pipe(
           catchError(this.handleError)
         )
   }
 
   editKlienci(data:addKlienci, id:number) {
     let API_URL = `${this.REST_API}/klienci/edit/${id}`;
     return this.httpClient.put(API_URL, data, { headers: this.httpHeaders })
       .pipe(
         catchError(this.handleError)
       )
   }
 
   getOneKlienci(id:number) {
     let API_URL = `${this.REST_API}/klienci/one/${id}`;
       return this.httpClient.get(API_URL)
         .pipe(
           catchError(this.handleError)
         )
   }
 
   handleError(error: HttpErrorResponse) {
     let errorMessage = '';
     if (error.error instanceof ErrorEvent) {
       // Handle client error
       errorMessage = error.error.message;
     } else {
       // Handle server error
       errorMessage = `Error Code: ${error.status}\nMessage: ${error.message}`;
     }
     console.log(errorMessage);
     return throwError(errorMessage);
   }
}
