import { HttpClient, HttpErrorResponse, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { throwError } from 'rxjs';
import { catchError, map } from 'rxjs/operators';
import { addProjekty } from '../addProjekty';
import { addWykSprzet } from '../addWykSprzet';

@Injectable({
  providedIn: 'root'
})
export class ProjektyService {

  // Node/Express API
  REST_API: string = 'http://localhost:8000/api';

  // Http Header
  httpHeaders = new HttpHeaders().set('Content-Type', 'application/json');

  constructor(private httpClient: HttpClient) { }

  getAllProjekty(){
    return this.httpClient.get(`${this.REST_API}/projekty/all`);
  }

  getAllPracownicyProjekty(id:number) {
    let API_URL = `${this.REST_API}/pracownicyprojekty/all/${id}`;
    return this.httpClient.get(API_URL, { headers: this.httpHeaders })
      .pipe(map((res: any) => {
          return res || {}
        }),
        catchError(this.handleError)
      )
  }

  getAllWykSprzetu(id:number) {
    let API_URL = `${this.REST_API}/wyksprzetu/all/${id}`;
    return this.httpClient.get(API_URL, { headers: this.httpHeaders })
      .pipe(map((res: any) => {
          return res || {}
        }),
        catchError(this.handleError)
      )
  }

  getAllUmowyProjekty(id:number) {
    let API_URL = `${this.REST_API}/umowyprojekty/all/${id}`;
    return this.httpClient.get(API_URL, { headers: this.httpHeaders })
      .pipe(map((res: any) => {
          return res || {}
        }),
        catchError(this.handleError)
      )
  }

  deactivateProjekty(id:number) {
    let API_URL = `${this.REST_API}/projekty/deactivate/${id}`;
    return this.httpClient.put(API_URL, { headers: this.httpHeaders })
      .pipe(
        catchError(this.handleError)
      )
  }

  deactivatePracownicyProjekty(id:number) {
    let API_URL = `${this.REST_API}/pracownicyprojekty/deactivate/${id}`;
    return this.httpClient.put(API_URL, { headers: this.httpHeaders })
      .pipe(
        catchError(this.handleError)
      )
  }

  deactivateWykSprzetu(id:number) {
    let API_URL = `${this.REST_API}/wyksprzetu/deactivate/${id}`;
    return this.httpClient.put(API_URL, { headers: this.httpHeaders })
      .pipe(
        catchError(this.handleError)
      )
  }

  deactivateUmowyProjekty(id:number) {
    let API_URL = `${this.REST_API}/umowyprojekty/deactivate/${id}`;
    return this.httpClient.put(API_URL, { headers: this.httpHeaders })
      .pipe(
        catchError(this.handleError)
      )
  }

  getUmowyDropdown() {
    return this.httpClient.get(`${this.REST_API}/umowyprojekty/umowy`);
  }

  checkUmowyProjekty(id: number, id2: number) {
    let API_URL = `${this.REST_API}/umowyprojekty/check/${id}/${id2}`;
    return this.httpClient.get(API_URL, { headers: this.httpHeaders })
      .pipe(
        catchError(this.handleError)
      )
  }

  addUmowyProjekty(id:number, id2: number) {
    let API_URL = `${this.REST_API}/umowyprojekty/add/${id}/${id2}`;
    return this.httpClient.post(API_URL, { headers: this.httpHeaders })
      .pipe(
        catchError(this.handleError)
      )
  }

  getPracownicyDropdown() {
    return this.httpClient.get(`${this.REST_API}/projekty/pracownicy`);
  }


  checkPracownicyProjekty(id:number, id2:number) {
    let API_URL = `${this.REST_API}/pracownicyprojekty/check/${id}/${id2}`;
    return this.httpClient.get(API_URL, { headers: this.httpHeaders })
      .pipe(
        catchError(this.handleError)
      )
  }
  
  addPracownicyProjekty(id:number, id2: number) {
    let API_URL = `${this.REST_API}/pracownicyprojekty/add/${id}/${id2}`;
    return this.httpClient.post(API_URL, { headers: this.httpHeaders })
      .pipe(
        catchError(this.handleError)
      )
  }

  addWykSprzet(data: addWykSprzet, id:number){
      let API_URL = `${this.REST_API}/wyksprzetu/add/${id}`;
      return this.httpClient.post(API_URL, data)
        .pipe(
          catchError(this.handleError)
        )
  }

  getSprzetDropdown() {
    return this.httpClient.get(`${this.REST_API}/wyksprzetu/sprzet`);
  }

  getDzialyDropdown() {
    return this.httpClient.get(`${this.REST_API}/wyksprzetu/dzialy`);
  }

  getOneWykSprzet(id: number) {
    let API_URL = `${this.REST_API}/wyksprzetu/one/${id}`;
      return this.httpClient.get(API_URL)
        .pipe(
          catchError(this.handleError)
        )
  }

  editWykSprzet(data: addWykSprzet, id:number) {
    let API_URL = `${this.REST_API}/wyksprzetu/edit/${id}`;
    return this.httpClient.put(API_URL, data, { headers: this.httpHeaders })
      .pipe(
        catchError(this.handleError)
      )
  }

  addProjekty(data:addProjekty) {
    let API_URL = `${this.REST_API}/projekty/add`;
      return this.httpClient.post(API_URL, data)
        .pipe(
          catchError(this.handleError)
        )
  }

  editProjekty(data:addProjekty, id:number) {
    let API_URL = `${this.REST_API}/projekty/edit/${id}`;
    return this.httpClient.put(API_URL, data, { headers: this.httpHeaders })
      .pipe(
        catchError(this.handleError)
      )
  }

  getOneProjekty(id:number) {
    let API_URL = `${this.REST_API}/projekty/one/${id}`;
      return this.httpClient.get(API_URL)
        .pipe(
          catchError(this.handleError)
        )
  }

  handleError(error: HttpErrorResponse) {
    let errorMessage = '';
    if (error.error instanceof ErrorEvent) {
      // Handle client error
      errorMessage = error.error.message;
    } else {
      // Handle server error
      errorMessage = `Error Code: ${error.status}\nMessage: ${error.message}`;
    }
    console.log(errorMessage);
    return throwError(errorMessage);
  }
}
