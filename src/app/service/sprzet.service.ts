import { HttpClient, HttpErrorResponse, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { addSprzet } from '../addSprzet';

@Injectable({
  providedIn: 'root'
})
export class SprzetService {

  // Node/Express API
  REST_API: string = 'http://localhost:8000/api';

  // Http Header
  httpHeaders = new HttpHeaders().set('Content-Type', 'application/json');
 
  constructor(private httpClient: HttpClient) { }
 
  getAllSprzet(){
    return this.httpClient.get(`${this.REST_API}/sprzet/all`);
  }
 
  
  deactivateSprzet(id:number) {
    let API_URL = `${this.REST_API}/sprzet/deactivate/${id}`;
    return this.httpClient.put(API_URL, { headers: this.httpHeaders })
      .pipe(
        catchError(this.handleError)
      )
  }

   getDzialyDropdown() {
     return this.httpClient.get(`${this.REST_API}/wyksprzetu/dzialy`);
   }
   
   addSprzet(data:addSprzet) {
     let API_URL = `${this.REST_API}/sprzet/add`;
       return this.httpClient.post(API_URL, data)
         .pipe(
           catchError(this.handleError)
         )
   }
 
   editSprzet(data:addSprzet, id:number) {
     let API_URL = `${this.REST_API}/sprzet/edit/${id}`;
     return this.httpClient.put(API_URL, data, { headers: this.httpHeaders })
       .pipe(
         catchError(this.handleError)
       )
   }
 
   getOneSprzet(id:number) {
     let API_URL = `${this.REST_API}/sprzet/one/${id}`;
       return this.httpClient.get(API_URL)
         .pipe(
           catchError(this.handleError)
         )
   }
 
   handleError(error: HttpErrorResponse) {
     let errorMessage = '';
     if (error.error instanceof ErrorEvent) {
       // Handle client error
       errorMessage = error.error.message;
     } else {
       // Handle server error
       errorMessage = `Error Code: ${error.status}\nMessage: ${error.message}`;
     }
     console.log(errorMessage);
     return throwError(errorMessage);
   }
}
