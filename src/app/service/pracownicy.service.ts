import { HttpClient, HttpErrorResponse, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { addPracownicy } from '../addPracownicy';

@Injectable({
  providedIn: 'root'
})
export class PracownicyService {

  // Node/Express API
  REST_API: string = 'http://localhost:8000/api';

  // Http Header
  httpHeaders = new HttpHeaders().set('Content-Type', 'application/json');
 
  constructor(private httpClient: HttpClient) { }
 
  getAllPracownicy(){
    return this.httpClient.get(`${this.REST_API}/pracownicy/all`);
  }
 
  
  deactivatePracownicy(id:number) {
    let API_URL = `${this.REST_API}/pracownicy/deactivate/${id}`;
    return this.httpClient.put(API_URL, { headers: this.httpHeaders })
      .pipe(
        catchError(this.handleError)
      )
  }

   getDzialyDropdown() {
     return this.httpClient.get(`${this.REST_API}/wyksprzetu/dzialy`);
   }
   
   addPracownicy(data:addPracownicy) {
     let API_URL = `${this.REST_API}/pracownicy/add`;
       return this.httpClient.post(API_URL, data)
         .pipe(
           catchError(this.handleError)
         )
   }
 
   editPracownicy(data:addPracownicy, id:number) {
     let API_URL = `${this.REST_API}/pracownicy/edit/${id}`;
     return this.httpClient.put(API_URL, data, { headers: this.httpHeaders })
       .pipe(
         catchError(this.handleError)
       )
   }
 
   getOnePracownicy(id:number) {
     let API_URL = `${this.REST_API}/pracownicy/one/${id}`;
       return this.httpClient.get(API_URL)
         .pipe(
           catchError(this.handleError)
         )
   }
 
   handleError(error: HttpErrorResponse) {
     let errorMessage = '';
     if (error.error instanceof ErrorEvent) {
       // Handle client error
       errorMessage = error.error.message;
     } else {
       // Handle server error
       errorMessage = `Error Code: ${error.status}\nMessage: ${error.message}`;
     }
     console.log(errorMessage);
     return throwError(errorMessage);
   }
}
