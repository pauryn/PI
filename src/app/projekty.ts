export class Projekty{
    constructor(
        public id: number,
        public nazwa: string,
        public deadline: Date,
        public cena: number,
        public dataRozp: Date,
        public dataZak: Date,
        public kierownik: string,
        public display: boolean
    ){}
}