import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ProjektyComponent } from './projekty/projekty.component';
import {LoginComponent} from './login/login.component';
import { KlienciComponent } from './klienci/klienci.component';
import { PracownicyComponent } from './pracownicy/pracownicy.component';
import { SprzetComponent } from './sprzet/sprzet.component';
import { DzialyComponent } from './dzialy/dzialy.component';

const routes: Routes = [
  { path: '', pathMatch: 'full', redirectTo: 'login'},
  { path: 'login', component: LoginComponent},
  { path: 'projekty', component: ProjektyComponent},
  { path: 'klienci', component: KlienciComponent},
  { path: 'pracownicy', component: PracownicyComponent},
  { path: 'sprzet', component: SprzetComponent},
  { path: 'dzialy', component: DzialyComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
