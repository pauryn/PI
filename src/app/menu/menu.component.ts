import { Component, Input, OnInit } from '@angular/core';
import { Location } from '@angular/common';
import { DomSanitizer } from '@angular/platform-browser';
import { Router } from '@angular/router';

@Component({
  selector: 'app-menu',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.scss']
})
export class MenuComponent implements OnInit {

  @Input() projektyColor: string = 'white';
  @Input() klienciColor: string = 'white';
  @Input() pracownicyColor: string = 'white';
  @Input() sprzetColor: string = 'white';
  @Input() dzialyColor: string = 'white';

  id: string;
  imie: string;
  nazwisko: string;
  dzial: string;
  zdjecie: any;

  constructor(
    private location:Location,
    private sanitizer: DomSanitizer,
    private router: Router,
  ) { }

  ngOnInit(): void {
    this.id = localStorage.getItem('id');
    
    if(parseInt(this.id)==0)
      this.router.navigateByUrl("/login");
    else {
      this.imie = localStorage.getItem('imie');
      this.nazwisko = localStorage.getItem('nazwisko');
      this.dzial = localStorage.getItem('dzial');
      this.zdjecie = this.sanitizer.bypassSecurityTrustResourceUrl('data:image/jpg;base64,'+ localStorage.getItem('zdjecie')); 
    }
    
  }

  logout(): void {
    localStorage.setItem('id', '0');
    localStorage.setItem('imie', '');
    localStorage.setItem('nazwisko', '');
    localStorage.setItem('dzial', '');
    localStorage.setItem('zdjecie','');
    this.router.navigateByUrl('/');
  }

}
