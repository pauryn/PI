import { Component, OnInit } from '@angular/core';
import { UmowyProjekty } from '../umowyprojekty';
import { PracownicyProjekty } from '../pracownicyprojekty';
import { Projekty } from '../projekty';
import { ProjektyService } from '../service/projekty.service';
import { WykSprzet } from '../wyksprzet';
import { animate, style, transition, trigger } from '@angular/animations';
import { DomSanitizer, SafeResourceUrl } from '@angular/platform-browser';
import { UmowyDropdown } from '../umowyDropdown';
import { PracownicyDropdown } from '../pracownicyDropdown';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { DzialyDropdown } from '../dzialyDropdown';
import { SprzetDropdown } from '../sprzetDropdown';
import { formatDate } from '@angular/common';

@Component({
  selector: 'app-projekty',
  templateUrl: './projekty.component.html',
  styleUrls: ['./projekty.component.scss'],
  animations: [
    trigger('fadeInOut', [
      transition(':enter', [   
        style({opacity:0}),
        animate(300, style({opacity:1})) 
      ]),
      transition(':leave', [   
        animate(300, style({opacity:0})) 
      ])
    ])
  ]
})
export class ProjektyComponent implements OnInit {
  
  wykSprzetForm: FormGroup;
  projektyForm: FormGroup;
  projekty: Array<Projekty> = [];
  displayedProjekty: Array<Projekty> = [];
  pracownicyProjekty: Array<PracownicyProjekty> = [];
  wykSprzet: Array<WykSprzet> = [];
  umowyProjekty: Array<UmowyProjekty> = [];
  umowyDropdown: Array<UmowyDropdown> = [];
  pracownicyDropdown: Array<PracownicyDropdown> = [];
  dzialyDropdown: Array<DzialyDropdown> = [];
  sprzetDropdown: Array<SprzetDropdown> = [];
  selectedProjekty: number;
  selectedProjektyName: string;
  isHidden: boolean = false;
  tableSize = '80vh';
  deactivate: boolean = false;
  toDeactivate : number;
  deactivateFunction: string;
  deactivateInfo: string;
  deactivated: boolean = false;
  photo: boolean = false;
  photoSrc:SafeResourceUrl;
  addUmowyProjekty:  boolean = false;
  selectedUmowy: number;
  selectedPracownicy:number;
  addPracownicyProjekty: boolean = false;
  addWykSprzet: boolean = false;
  editWykSprzet: boolean = false;
  editedWykSprzet:number;
  addProjekty: boolean = false;
  EditProjekty: boolean = false;
  editedProjekty: number;
  
  constructor(
    private projektyService: ProjektyService, 
    private sanitizer: DomSanitizer,
    private formBuilder: FormBuilder
    ) {
      this.wykSprzetForm = this.formBuilder.group({
        dz: ['', Validators.required],
        sp: ['', Validators.required],
        rozp: ['', Validators.required],
        zak: ['', Validators.required],
      })
      this.projektyForm = this.formBuilder.group({
        pr: ['', Validators.required],
        nazwa: ['', Validators.required],
        deadline: ['', Validators.required],
        cena: ['', Validators.required],
        rozp: ['', Validators.required],
        zak: ['', Validators.required],
      })
     }


  

  ngOnInit(): void {
    this.getAllProjekty();
  }

  getAllProjekty() : void {
    this.projekty = [];
    this.projektyService.getAllProjekty().subscribe(res => {
      for(let i=0; i<Object.keys(res).length; i++)
        this.projekty.push(new Projekty(res[i].PROJ_Id, res[i].PROJ_Nazwa, res[i].PROJ_Deadline, res[i].PROJ_Cena, res[i].PROJ_Data_Rozp, res[i].PROJ_Data_Zak, res[i].Pracownik, true));
      
    })
  }

  selectProjekty(id:number, name: string): void {
    this.isHidden = false;

    this.selectedProjekty=id;
    this.selectedProjektyName=name;
     
    this.tableSize='40vh';
    
    this.getAllPracownicyProjekty(id);
    this.getAllWykSprzetu(id);
    this.getAllUmowyProjekty(id);
    
    this.isHidden=true;
  }

  getAllPracownicyProjekty(id : number) {
    this.pracownicyProjekty=[];
    this.projektyService.getAllPracownicyProjekty(id).subscribe(res => {
      for(let i=0; i<Object.keys(res).length; i++){
        let binary = '';
        let bytes = new Uint8Array(res[i].PR_Zdjecie.data);
        let len = bytes.byteLength;
        for (let j = 0; j < len; j++) 
          binary += String.fromCharCode( bytes[j] );
        
        res[i].PR_Zdjecie = btoa(binary);
        this.pracownicyProjekty.push(new PracownicyProjekty(res[i].PP_Id, res[i].PR_Imie, res[i].PR_Nazwisko, res[i].DZ_Nazwa, res[i].PR_Telefon, res[i].PR_Zdjecie, true));
      }
    });
  }

  getAllWykSprzetu(id: number) {
    this.wykSprzet=[];
    this.projektyService.getAllWykSprzetu(id).subscribe(res => {
      for(let i=0; i<Object.keys(res).length; i++){
        let binary = '';
        let bytes = new Uint8Array(res[i].SP_Zdjecie.data);
        let len = bytes.byteLength;
        for (let j = 0; j < len; j++) 
          binary += String.fromCharCode( bytes[j] );
        
        res[i].SP_Zdjecie = btoa(binary);

        this.wykSprzet.push(new WykSprzet(res[i].WYK_Id, res[i].SP_Nazwa, res[i].DZ_Nazwa, res[i].WYK_Data_Rozp, res[i].WYK_Data_Zak, res[i].SP_Zdjecie, true));
      }
    });
  }

  getAllUmowyProjekty(id: number) {
    this.umowyProjekty=[];
    this.projektyService.getAllUmowyProjekty(id).subscribe(res=> {
      for(let i=0; i<Object.keys(res).length; i++){
        let binary = '';
        let bytes = new Uint8Array(res[i].UM_Zdjecie.data);
        let len = bytes.byteLength;
        for (let j = 0; j < len; j++) 
          binary += String.fromCharCode( bytes[j] );
        
        res[i].UM_Zdjecie = btoa(binary);
        this.umowyProjekty.push(new UmowyProjekty(res[i].UP_Id, res[i].KL_Firma, res[i].UM_Data_zaw, res[i].UM_Data_zak, res[i].UM_Cena, res[i].UM_Zdjecie, true));
      }
      
    });
  }

  deactivatePopUp(id:number, f:string): void{
    this.deactivate = true;
    this.toDeactivate = id;
    this.deactivateFunction = f;

  }

  hideDeactivate(): void{
    this.deactivate = false;
    this.deactivated = false;
    this.toDeactivate = 0;
    this.deactivateFunction = '';
  }

  submitDeactivate(): void{
    if(this.deactivateFunction=='projekty') {
      this.projektyService.deactivateProjekty(this.toDeactivate).subscribe(res => {
          this.deactivateInfo='Poprawnie usunięto dane.';
          this.getAllProjekty();
        }, (err) => {
          this.deactivateInfo='Wystąpił błąd';
        });
    }
    if(this.deactivateFunction=='pracownicyProjekty') {
      this.projektyService.deactivatePracownicyProjekty(this.toDeactivate).subscribe(res => {
          this.deactivateInfo='Poprawnie usunięto dane.';
          this.getAllPracownicyProjekty(this.selectedProjekty);
        }, (err) => {
          this.deactivateInfo='Wystąpił błąd';
        });
    }
    if(this.deactivateFunction=='wykSprzet') {
      this.projektyService.deactivateWykSprzetu(this.toDeactivate).subscribe(res => {
          this.deactivateInfo='Poprawnie usunięto dane.';
          this.getAllWykSprzetu(this.selectedProjekty);
        }, (err) => {
          this.deactivateInfo='Wystąpił błąd';
        });
    }
    if(this.deactivateFunction=='umowyProjekty') {
      this.projektyService.deactivateUmowyProjekty(this.toDeactivate).subscribe(res => {
          this.deactivateInfo='Poprawnie usunięto dane.';
          this.getAllUmowyProjekty(this.selectedProjekty);
        }, (err) => {
          this.deactivateInfo='Wystąpił błąd';
        });
    }
    this.deactivate=false;
    this.deactivated=true;
  }

  showUmowyProjekty() {
    this.umowyDropdown = [];
    this.projektyService.getUmowyDropdown().subscribe(res=> {
      for(let i=0; i<Object.keys(res).length; i++){
        
        this.umowyDropdown.push(new UmowyDropdown(res[i].UM_Id, res[i].umowa));
      }
    });
    this.addUmowyProjekty = true;
  }

  closeUmowyProjekty() {
    this.addUmowyProjekty = false;

  }
  
  submitUmowyProjekty() {
    this.projektyService.checkUmowyProjekty(this.selectedProjekty, this.selectedUmowy).subscribe(res => {
        if(res[0].RESULT>0){
          this.deactivateInfo='Ta Umowa została już wcześniej dodana do tego projektu.'
        }
        else{
          this.projektyService.addUmowyProjekty(this.selectedProjekty, this.selectedUmowy).subscribe(res2 =>{
            this.deactivateInfo='Poprawnie dodano dane';
            this.getAllUmowyProjekty(this.selectedProjekty);
          },(err) => {
            this.deactivateInfo='Wystąpił błąd';
          });
        }
      }, (err) => {
          this.deactivateInfo='Wystąpił błąd';
    });
    this.addUmowyProjekty = false;
    this.deactivated=true;
  }

  showPracownicyProjekty() {
    this.pracownicyDropdown=[];
    this.projektyService.getPracownicyDropdown().subscribe(res=> {
      for(let i=0; i<Object.keys(res).length; i++){
        
        this.pracownicyDropdown.push(new PracownicyDropdown(res[i].PR_ID, res[i].pracownik));
      }
    });
    this.addPracownicyProjekty = true;
  }

  closePracownicyProjekty() {
    this.addPracownicyProjekty = false;

  }
  
  submitPracownicyProjekty() {
    this.projektyService.checkPracownicyProjekty(this.selectedProjekty, this.selectedPracownicy).subscribe(res => {
        if(res[0].RESULT>0){
          this.deactivateInfo='Ten Pracownik został już wcześniej dodany do tego projektu.'
        }
        else{
          this.projektyService.addPracownicyProjekty(this.selectedProjekty, this.selectedPracownicy).subscribe(res2 =>{
            this.deactivateInfo='Poprawnie dodano dane';
            this.getAllPracownicyProjekty(this.selectedProjekty);
          },(err) => {
            this.deactivateInfo='Wystąpił błąd';
          });
        }
      }, (err) => {
          this.deactivateInfo='Wystąpił błąd';
    });
    this.addPracownicyProjekty = false;
    this.deactivated=true;
  }

  openPhoto(photo: string): void {
    this.photoSrc=this.sanitizer.bypassSecurityTrustResourceUrl('data:image/jpg;base64,'+ photo); 
    this.photo = true;
  }
  closePhoto(): void {
    this.photoSrc='';
    this.photo=false;
  }

  showWykSprzet(): void {
    this.sprzetDropdown=[];
    this.dzialyDropdown=[];
    this.wykSprzetForm.controls.dz.setValue(null);
    this.wykSprzetForm.controls.sp.setValue(null);
    this.wykSprzetForm.controls.rozp.setValue(null);
    this.wykSprzetForm.controls.zak.setValue(null);
    this.projektyService.getSprzetDropdown().subscribe(res=> {
      for(let i=0; i<Object.keys(res).length; i++){
        
        this.sprzetDropdown.push(new SprzetDropdown(res[i].SP_Id, res[i].SP_Nazwa));
      }
    });
    this.projektyService.getDzialyDropdown().subscribe(res=> {
      for(let i=0; i<Object.keys(res).length; i++){
        this.dzialyDropdown.push(new DzialyDropdown(res[i].DZ_Id, res[i].DZ_Nazwa));
      }
    });
    this.addWykSprzet= true;
  }

  submitWykSprzet():void {
    this.projektyService.addWykSprzet(this.wykSprzetForm.value, this.selectedProjekty).subscribe(() => {
        this.deactivateInfo='Poprawnie dodano Wykorzystanie Sprzętu';
        this.getAllWykSprzetu(this.selectedProjekty);
        
      }, (err) => {
        this.deactivateInfo='Wystąpił błąd';
    });
    this.addWykSprzet=false;
    this.deactivated=true;
  }

  closeWykSprzet():void {
    this.addWykSprzet=false;
  }

  showEditWykSprzet(id: number): void {
    this.sprzetDropdown=[];
    this.dzialyDropdown=[];
    this.editedWykSprzet=id;
    this.projektyService.getSprzetDropdown().subscribe(res=> {
      for(let i=0; i<Object.keys(res).length; i++){
        
        this.sprzetDropdown.push(new SprzetDropdown(res[i].SP_Id, res[i].SP_Nazwa));
      }
    });
    this.projektyService.getDzialyDropdown().subscribe(res=> {
      for(let i=0; i<Object.keys(res).length; i++){
        this.dzialyDropdown.push(new DzialyDropdown(res[i].DZ_Id, res[i].DZ_Nazwa));
      }
    });
    this.projektyService.getOneWykSprzet(id).subscribe(res=>{
      this.wykSprzetForm.controls.sp.setValue(res[0].SP_Id);
      this.wykSprzetForm.controls.dz.setValue(res[0].DZ_Id);
      this.wykSprzetForm.controls.rozp.setValue(formatDate(res[0].WYK_Data_Rozp, "yyyy-MM-dd", 'pl-PL'));
      this.wykSprzetForm.controls.zak.setValue(formatDate(res[0].WYK_Data_Zak, "yyyy-MM-dd", 'pl-PL'));
    })
    this.editWykSprzet= true;
  }

  submitEditWykSprzet():void {
    this.projektyService.editWykSprzet(this.wykSprzetForm.value, this.editedWykSprzet).subscribe(() => {
        this.deactivateInfo='Poprawnie edytowano Wykorzystanie Sprzętu';
        this.getAllWykSprzetu(this.selectedProjekty);
        
      }, (err) => {
        this.deactivateInfo='Wystąpił błąd';
        console.log(err);
    });
    this.editWykSprzet=false;
    this.deactivated=true;
  }

  closeEditWykSprzet():void {
    this.editWykSprzet=false;
  }

  showAddProjekty(){
    this.pracownicyDropdown=[];
    this.projektyForm.controls.pr.setValue(null);
    this.projektyForm.controls.nazwa.setValue(null);
    this.projektyForm.controls.rozp.setValue(null);
    this.projektyForm.controls.zak.setValue(null);
    this.projektyForm.controls.deadline.setValue(null);
    this.projektyForm.controls.cena.setValue(null);
    this.projektyService.getPracownicyDropdown().subscribe(res=> {
      for(let i=0; i<Object.keys(res).length; i++){
        
        this.pracownicyDropdown.push(new PracownicyDropdown(res[i].PR_ID, res[i].pracownik));
      }
    });
    this.addProjekty= true;
  }

  closeAddProjekty():void {
    this.addProjekty=false;
  }

  submitAddProjekty():void {
    this.projektyService.addProjekty(this.projektyForm.value).subscribe(() => {
      this.deactivateInfo='Poprawnie dodano nowy Projekt.';
      this.getAllProjekty();
      
    }, (err) => {
      this.deactivateInfo='Wystąpił błąd';
    });
    this.addProjekty=false;
    this.deactivated=true;
  }

  showEditProjekty(id: number): void {
    this.pracownicyDropdown=[];
    this.editedProjekty=id;
    this.projektyService.getPracownicyDropdown().subscribe(res=> {
      for(let i=0; i<Object.keys(res).length; i++){
        
        this.pracownicyDropdown.push(new PracownicyDropdown(res[i].PR_ID, res[i].pracownik));
      }
    });
    this.projektyService.getOneProjekty(id).subscribe(res=>{
      this.projektyForm.controls.pr.setValue(res[0].PR_Id);
      this.projektyForm.controls.nazwa.setValue(res[0].PROJ_Nazwa);
      this.projektyForm.controls.rozp.setValue(formatDate(res[0].PROJ_Data_Rozp, "yyyy-MM-dd", 'pl-PL'));
      this.projektyForm.controls.zak.setValue(formatDate(res[0].PROJ_Data_Zak, "yyyy-MM-dd", 'pl-PL'));
      this.projektyForm.controls.deadline.setValue(formatDate(res[0].PROJ_Deadline, "yyyy-MM-dd", 'pl-PL'));
      this.projektyForm.controls.cena.setValue(res[0].PROJ_Cena);;
    })
    this.EditProjekty= true;
  }

  submitEditProjekty():void {
    this.projektyService.editProjekty(this.projektyForm.value, this.editedProjekty).subscribe(() => {
        this.deactivateInfo='Poprawnie edytowano Projekt';
        this.getAllProjekty();
        
      }, (err) => {
        this.deactivateInfo='Wystąpił błąd';
        console.log(err);
    });
    this.EditProjekty=false;
    this.deactivated=true;
  }

  closeEditProjekty():void {
    this.EditProjekty=false;
  }

  searchProjekty(event: any){
      if (event.target.value) {
        let text = event.target.value.toLowerCase();
        let filtered = this.projekty.map(i => { return i.nazwa + ' ' + i.kierownik + ' ' + formatDate(i.deadline, "dd-MM-yyyy", 'pl-PL') + ' ' + i.cena + ' ' + formatDate(i.dataRozp, "dd-MM-yyyy", 'pl-PL') + ' ' + formatDate(i.dataZak, "dd-MM-yyyy", 'pl-PL')});
        filtered.filter((i, index) => {
          if(i.toLowerCase().includes(text)) 
            this.projekty[index].display = true;
          else
            this.projekty[index].display = false;
        });
      }
      else
        this.projekty.forEach( i => i.display = true);
  }

  searchPracownicyProjekty(event: any){
    
    if (event.target.value) {
      
      let text = event.target.value.toLowerCase();
      let filtered = this.pracownicyProjekty.map(i => { return i.imie + ' ' + i.nazwisko + ' ' +  i.telefon + ' ' + i.dzial});
      filtered.filter((i, index) => {
        if(i.toLowerCase().includes(text)) 
          this.pracownicyProjekty[index].display = true;
        else
          this.pracownicyProjekty[index].display = false;
      });
    }
    else
      this.pracownicyProjekty.forEach( i => i.display = true);
  } 

  searchWykSprzet(event: any){
    if (event.target.value) {
      let text = event.target.value.toLowerCase();
      let filtered = this.wykSprzet.map(i => { return i.sprzet + ' ' + i.dzial + ' ' + formatDate(i.dataRozp, "dd-MM-yyyy", 'pl-PL') + ' ' + formatDate(i.dataZak, "dd-MM-yyyy", 'pl-PL')});
      filtered.filter((i, index) => {
        if(i.toLowerCase().includes(text)) 
          this.wykSprzet[index].display = true;
        else
          this.wykSprzet[index].display = false;
      });
    }
    else
      this.wykSprzet.forEach( i => i.display = true);
  }

  searchUmowyProjekty(event: any){
    if (event.target.value) {
      let text = event.target.value.toLowerCase();
      let filtered = this.umowyProjekty.map(i => { return i.firma + ' ' + i.cena + ' ' + formatDate(i.dataZaw, "dd-MM-yyyy", 'pl-PL') + ' ' + formatDate(i.dataZak, "dd-MM-yyyy", 'pl-PL')});
      filtered.filter((i, index) => {
        if(i.toLowerCase().includes(text)) 
          this.umowyProjekty[index].display = true;
        else
          this.umowyProjekty[index].display = false;
      });
    
    }
    else
      this.umowyProjekty.forEach( i => i.display = true);
  }
}
