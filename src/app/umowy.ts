export class Umowy{
    constructor(
        public id: number,
        public klient: number,
        public cena: string,
        public dataZaw: Date,
        public dataZak: Date,
        public zdjecie: string,
        public display: boolean
    ){}
}