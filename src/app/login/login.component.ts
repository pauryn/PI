import { animate, state, style, transition, trigger } from '@angular/animations';
import { ChangeDetectionStrategy, Component, OnInit } from '@angular/core';
import { Router} from '@angular/router';
import { loginpassword } from '../login';
import { LoginService } from '../service/login.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss'],
  animations: [
    trigger('state', [
      state(
        'visible',
        style({
          opacity: '1'
        })
      ),
      state(
        'hidden',
        style({
          opacity: '0'
        })
      ),
      transition('* => visible', [
        style({ opacity: 0, transform: 'translateY(10px)' }),
        animate('500ms', style({ opacity: 1, transform: 'translateY(0)' })),
      ])
    ]),
  ],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class LoginComponent implements OnInit {
  
  title = 'Firma Deweloperska';
  login:  any;
  password: any;
  loginError = '';
  passwordError = '';
  allError ='';
  loginBorderColor ='#0297D7';
  passwordBorderColor ='#0297D7';

  constructor(
    private loginService: LoginService,
    private router: Router
  ) { }

  ngOnInit(): void {
    localStorage.setItem('id', '0');
  }

  Login(login, password): void {
    this.loginError = '';
    this.passwordError = '';
    this.allError='';
    this.loginBorderColor='#0297D7';
    this.passwordBorderColor='#0297D7';
    if(login.length>0 && password.length>0) {
      this.loginService.CheckData(new loginpassword(login, password)).subscribe(res => {
        if (res.result==true) {
          localStorage.setItem('id', res.id);
          localStorage.setItem('imie', res.imie);
          localStorage.setItem('nazwisko', res.nazwisko);
          localStorage.setItem('dzial', res.dzial);
          
          let binary = '';
          let bytes = new Uint8Array(res.zdjecie.data);
	        let len = bytes.byteLength;
	        for (let i = 0; i < len; i++) {
		        binary += String.fromCharCode( bytes[i] );
	        }
          res.zdjecie = btoa(binary);
          localStorage.setItem('zdjecie', res.zdjecie);
          this.router.navigateByUrl("/projekty");
        }
        else {
          this.allError='Niepoprawny Login i/lub Hasło';
          this.loginBorderColor='red';
          this.passwordBorderColor='red';
        }
      
      })
    }
    else {
      if(login.length==0) {
        this.loginError='Pole wymagane';
        this.loginBorderColor='red';
      }
      if(password.length==0) {
        this.passwordError='Pole wymagane';
        this.passwordBorderColor='red';
      }

    }
  }
  
  

}
