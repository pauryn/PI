export class klienci{
    constructor(
        public id: number,
        public firma: string,
        public nip: string,
        public adres: string,
        public telefon: string,
        public display: boolean
    ){}
}