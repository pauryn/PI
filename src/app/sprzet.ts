export class Sprzet{
    constructor(
        public id: number,
        public nazwa: string,
        public dzial: string,
        public dataWpr: Date,
        public dataWyc: Date,
        public zdjecie: string,
        public display: boolean
    ){}
}