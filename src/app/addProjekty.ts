export class addProjekty{
    constructor(
        public pr: number,
        public nazwa: string,
        public deadline: Date,
        public cena: number,
        public rozp: Date,
        public zak: Date
    ){}
}