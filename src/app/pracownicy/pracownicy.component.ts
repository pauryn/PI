import { animate, style, transition, trigger } from '@angular/animations';
import { formatDate } from '@angular/common';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { DomSanitizer, SafeResourceUrl } from '@angular/platform-browser';
import { DzialyDropdown } from '../dzialyDropdown';
import { Pracownicy } from '../pracownicy';
import { PracownicyService } from '../service/pracownicy.service';

@Component({
  selector: 'app-pracownicy',
  templateUrl: './pracownicy.component.html',
  styleUrls: ['./pracownicy.component.scss'],
  animations: [
    trigger('fadeInOut', [
      transition(':enter', [   
        style({opacity:0}),
        animate(300, style({opacity:1})) 
      ]),
      transition(':leave', [   
        animate(300, style({opacity:0})) 
      ])
    ])
  ]
})
export class PracownicyComponent implements OnInit {

  pracownicyForm: FormGroup;
  tableSize = '80vh';
  pracownicy: Array<Pracownicy> = [];
  dzialyDropdown: Array<DzialyDropdown> = [];
  deactivate: boolean = false;
  toDeactivate : number;
  deactivateInfo: string;
  deactivated: boolean = false;
  photo: boolean = false;
  photoSrc:SafeResourceUrl;
  addPracownicy: boolean = false;
  editPracownicy: boolean = false;
  editedPracownicy: number;
  editedPhoto: any;

  constructor(
    private pracownicyService: PracownicyService,
    private sanitizer: DomSanitizer,
    private formBuilder: FormBuilder
  ) {
    this.pracownicyForm = this.formBuilder.group({
      dz: ['', Validators.required],
      zdj:  '',
      imie:  ['', Validators.required],
      nazw:  ['', Validators.required],
      ur:  ['', Validators.required],
      adres:  ['', Validators.required],
      zatr:  ['', Validators.required],
      zwol: null,
      pesel:  ['', Validators.required],
      tel:  ['', Validators.required]
    })
  }

  ngOnInit(): void {
    this.getAllPracownicy();
  }

  getAllPracownicy() : void {
    this.pracownicy = [];
    this.pracownicyService.getAllPracownicy().subscribe(res => {
      console.log(res)
      for(let i=0; i<Object.keys(res).length; i++) {
        let binary = '';
        let bytes = new Uint8Array(res[i].PR_Zdjecie.data);
        let len = bytes.byteLength;
        for (let j = 0; j < len; j++) 
          binary += String.fromCharCode( bytes[j] );
        
        res[i].PR_Zdjecie = btoa(binary);
        this.pracownicy.push(new Pracownicy(res[i].PR_Id, res[i].PR_Imie, res[i].PR_Nazwisko, res[i].DZ_Nazwa, res[i].PR_Telefon, res[i].PR_Data_Zatr, res[i].PR_Data_Zwol, res[i].PR_Data_Ur, res[i].PR_Adres, res[i].PR_Pesel, res[i].PR_Zdjecie, true));
      }
    })
  }

  deactivatePopUp(id:number): void{
    this.deactivate = true;
    this.toDeactivate = id;

  }

  hideDeactivate(): void{
    this.deactivate = false;
    this.deactivated = false;
    this.toDeactivate = 0;
  }

  submitDeactivate(): void{
    this.pracownicyService.deactivatePracownicy(this.toDeactivate).subscribe(res => {
          this.deactivateInfo='Poprawnie usunięto dane.';
          this.getAllPracownicy();
        }, (err) => {
          this.deactivateInfo='Wystąpił błąd';
        });
    this.deactivate=false;
    this.deactivated=true;
  }

  openPhoto(photo: string): void {
    this.photoSrc=this.sanitizer.bypassSecurityTrustResourceUrl('data:image/jpg;base64,'+ photo); 
    this.photo = true;
  }

  closePhoto(): void {
    this.photoSrc='';
    this.photo=false;
  }

  showAddPracownicy(): void {
    this.dzialyDropdown=[];
    this.pracownicyForm.controls.dz.setValue(null);
    this.pracownicyForm.controls.imie.setValue(null);
    this.pracownicyForm.controls.zdj.setValue(null);
    this.pracownicyForm.controls.nazw.setValue(null);
    this.pracownicyForm.controls.ur.setValue(null);
    this.pracownicyForm.controls.adres.setValue(null);
    this.pracownicyForm.controls.zatr.setValue(null);
    this.pracownicyForm.controls.zwol.setValue(null);
    this.pracownicyForm.controls.pesel.setValue(null);
    this.pracownicyForm.controls.tel.setValue(null);
    this.pracownicyService.getDzialyDropdown().subscribe(res=> {
      for(let i=0; i<Object.keys(res).length; i++){
        
        this.dzialyDropdown.push(new DzialyDropdown(res[i].DZ_Id, res[i].DZ_Nazwa));
      }
    });
    this.addPracownicy= true;
  }

  submitAddPracownicy():void {
    let binary = '';
    let bytes = new Uint8Array(this.pracownicyForm.value.zdj);
    let len = bytes.byteLength;
    for (let j = 0; j < len; j++) 
      binary += String.fromCharCode( bytes[j] );
    this.pracownicyForm.value.zdj = btoa(binary);
    console.log(this.pracownicyForm.value.zdj);
    this.pracownicyService.addPracownicy(this.pracownicyForm.value).subscribe(() => {
      this.deactivateInfo='Poprawnie dodano Pracownika';
      this.getAllPracownicy();
       
    }, (err) => {
        this.deactivateInfo='Wystąpił błąd';
    });
    this.addPracownicy=false;
    this.deactivated=true;
  }

  closeAddPracownicy():void {
    this.addPracownicy=false;
  }

  showEditPracownicy(id: number, photo: string): void {
    this.dzialyDropdown=[];
    this.editedPracownicy=id;
    console.log(id);
    this.editedPhoto = photo;
    this.pracownicyService.getDzialyDropdown().subscribe(res=> {
      for(let i=0; i<Object.keys(res).length; i++){
        
        this.dzialyDropdown.push(new DzialyDropdown(res[i].DZ_Id, res[i].DZ_Nazwa));
      }
    });
    this.pracownicyService.getOnePracownicy(id).subscribe(res=>{
      this.pracownicyForm.controls.dz.setValue(res[0].DZ_Id);
      this.pracownicyForm.controls.imie.setValue(res[0].PR_Imie);
      this.pracownicyForm.controls.nazw.setValue(res[0].PR_Nazwisko);
      this.pracownicyForm.controls.ur.setValue(formatDate(res[0].PR_Data_Ur, "yyyy-MM-dd", 'pl-PL'));
      this.pracownicyForm.controls.adres.setValue(res[0].PR_Adres);
      this.pracownicyForm.controls.zatr.setValue(formatDate(res[0].PR_Data_Zatr, "yyyy-MM-dd", 'pl-PL'));
     
      this.pracownicyForm.controls.pesel.setValue(res[0].PR_Pesel);
      this.pracownicyForm.controls.tel.setValue(res[0].PR_Telefon);
    })
    this.editPracownicy= true;
  }
  submitEditPracownicy():void {
    if(this.pracownicyForm.value.zdj != '') {
      let binary = '';
      let bytes = new Uint8Array(this.pracownicyForm.value.zdj);
      let len = bytes.byteLength;
      for (let j = 0; j < len; j++) 
        binary += String.fromCharCode( bytes[j] );
      this.pracownicyForm.value.zdj = btoa(binary);
    }
    console.log(this.pracownicyForm.value)
    this.pracownicyService.editPracownicy(this.pracownicyForm.value, this.editedPracownicy).subscribe(() => {
      this.deactivateInfo='Poprawnie edytowano Pracownika';
      this.getAllPracownicy();
      
      }, (err) => {
        this.deactivateInfo='Wystąpił błąd';
        console.log(err);
    });
    this.editPracownicy=false;
    this.deactivated=true;
  }

  closeEditPracownicy():void {
    this.editPracownicy=false;
  }

  searchPracownicy(event: any){
    if (event.target.value) {
      let text = event.target.value.toLowerCase();
      let filtered = this.pracownicy.map(i => { return i.imie + ' ' + i.nazwisko + ' ' + i.dzial + ' ' + i.telefon + ' ' + formatDate(i.dataZatr, "dd.MM.yyyy", 'pl-PL') + ' ' + formatDate(i.dataZwol, "dd.MM.yyyy", 'pl-PL') + ' ' + formatDate(i.dataUr, "dd.MM.yyyy", 'pl-PL') + ' ' + i.adres + ' ' + i.pesel});
      filtered.filter((i, index) => {
        if(i.toLowerCase().includes(text)) 
          this.pracownicy[index].display = true;
        else
          this.pracownicy[index].display = false;
      });
    }
    else
      this.pracownicy.forEach( i => i.display = true);
  }

  onFileChange(event) {    
    if (event.target.files.length > 0) {
      const file = event.target.files[0];
      const reader = new FileReader();
      
      reader.onload = () => { 
        this.pracownicyForm.value.zdj = reader.result;
      }
      reader.readAsArrayBuffer(file);
     
    }
  }
}
