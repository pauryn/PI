export class Pracownicy{
    constructor(
        public id: number,
        public imie: string,
        public nazwisko: string,
        public dzial: string,
        public telefon: number,
        public dataZatr: Date,
        public dataZwol: Date,
        public dataUr: Date,
        public adres: string,
        public pesel: number,
        public zdjecie: string,
        public display: boolean
    ){}
}