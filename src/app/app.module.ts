import { HttpClientModule } from '@angular/common/http';
import { DEFAULT_CURRENCY_CODE, LOCALE_ID, NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser';
import { MatIconModule } from '@angular/material/icon';
import localePl from '@angular/common/locales/pl';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { ProjektyComponent } from './projekty/projekty.component';
import { LoginComponent } from './login/login.component';
import { MenuComponent } from './menu/menu.component';
import { registerLocaleData } from '@angular/common';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import { KlienciComponent } from './klienci/klienci.component';
import { PracownicyComponent } from './pracownicy/pracownicy.component';
import { SprzetComponent } from './sprzet/sprzet.component';
import { DzialyComponent } from './dzialy/dzialy.component';

registerLocaleData(localePl);

@NgModule({
  declarations: [
    AppComponent,
    ProjektyComponent,
    LoginComponent,
    MenuComponent,
    KlienciComponent,
    PracownicyComponent,
    SprzetComponent,
    DzialyComponent
    ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    HttpClientModule,
    MatIconModule,
    BrowserAnimationsModule,
    FormsModule,
    ReactiveFormsModule,
    NgbModule
  ],
  providers: [
    { provide: LOCALE_ID, useValue: "pl-PL" },
    {provide: DEFAULT_CURRENCY_CODE, useValue: 'PLN'}
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
