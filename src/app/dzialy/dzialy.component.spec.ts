import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DzialyComponent } from './dzialy.component';

describe('DzialyComponent', () => {
  let component: DzialyComponent;
  let fixture: ComponentFixture<DzialyComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DzialyComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DzialyComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
