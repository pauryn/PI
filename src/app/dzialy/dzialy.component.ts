import { trigger, transition, style, animate } from '@angular/animations';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Dzialy } from '../dzialy';
import { PracownicyDropdown } from '../pracownicyDropdown';
import { DzialyService } from '../service/dzialy.service';

@Component({
  selector: 'app-dzialy',
  templateUrl: './dzialy.component.html',
  styleUrls: ['./dzialy.component.scss'],
  animations: [
    trigger('fadeInOut', [
      transition(':enter', [   
        style({opacity:0}),
        animate(300, style({opacity:1})) 
      ]),
      transition(':leave', [   
        animate(300, style({opacity:0})) 
      ])
    ])
  ]
})
export class DzialyComponent implements OnInit {

  dzialyForm: FormGroup;
  tableSize = '80vh';
  dzialy: Array<Dzialy> = [];
  pracownicyDropdown: Array<PracownicyDropdown> = [];
  deactivate: boolean = false;
  toDeactivate : number;
  deactivateInfo: string;
  deactivated: boolean = false;
  addDzialy: boolean = false;
  editDzialy: boolean = false;
  editedDzialy: number;

  constructor(
    private dzialyService: DzialyService,
    private formBuilder: FormBuilder
  ) { 
    this.dzialyForm = this.formBuilder.group({
      pr: ['', Validators.required],
      nazwa: ['', Validators.required]
    })
  }

  ngOnInit(): void {
    this.getAllDzialy();
  }

  getAllDzialy() : void {
    this.dzialy = [];
    this.dzialyService.getAllDzialy().subscribe(res => {
      for(let i=0; i<Object.keys(res).length; i++) {
        this.dzialy.push(new Dzialy(res[i].DZ_Id,res[i].DZ_Nazwa, res[i].pracownik, true));
      }
    })
  }

  deactivatePopUp(id:number): void{
    this.deactivate = true;
    this.toDeactivate = id;

  }

  hideDeactivate(): void{
    this.deactivate = false;
    this.deactivated = false;
    this.toDeactivate = 0;
  }

  submitDeactivate(): void{
    this.dzialyService.deactivateDzialy(this.toDeactivate).subscribe(res => {
          this.deactivateInfo='Poprawnie usunięto dane.';
          this.getAllDzialy();
        }, (err) => {
          this.deactivateInfo='Wystąpił błąd';
        });
    this.deactivate=false;
    this.deactivated=true;
  }

  showAddDzialy(): void {
    this.pracownicyDropdown=[];
    this.dzialyForm.controls.pr.setValue(null);
    this.dzialyForm.controls.nazwa.setValue(null);
    this.dzialyService.getPracownicyDropdown().subscribe(res=> {
      for(let i=0; i<Object.keys(res).length; i++){
        
        this.pracownicyDropdown.push(new PracownicyDropdown(res[i].PR_ID, res[i].pracownik));
      }
    });
    this.addDzialy= true;
  }

  submitAddDzialy(): void {
    this.dzialyService.addDzialy(this.dzialyForm.value).subscribe(() => {
      this.deactivateInfo='Poprawnie dodano Dział';
      this.getAllDzialy();
       
    }, (err) => {
        this.deactivateInfo='Wystąpił błąd';
    });
    this.addDzialy=false;
    this.deactivated=true;
  }

  closeAddDzialy():void {
    this.addDzialy=false;
  }

  showEditDzialy(id: number): void {
    this.pracownicyDropdown=[];
    this.editedDzialy = id;
    this.dzialyService.getPracownicyDropdown().subscribe(res=> {
      console.log(res);
      for(let i=0; i<Object.keys(res).length; i++){
        
        this.pracownicyDropdown.push(new PracownicyDropdown(res[i].PR_ID, res[i].pracownik));
      }
    });
    this.dzialyService.getOneDzialy(id).subscribe(res=>{
      console.log(res);
      this.dzialyForm.controls.pr.setValue(res[0].PR_Id);
      this.dzialyForm.controls.nazwa.setValue(res[0].DZ_Nazwa);
     })
    this.editDzialy= true;
  }
  submitEditDzialy():void {
    this.dzialyService.editDzialy(this.dzialyForm.value, this.editedDzialy).subscribe(() => {
      this.deactivateInfo='Poprawnie edytowano Dział';
      this.getAllDzialy();
      
      }, (err) => {
        this.deactivateInfo='Wystąpił błąd';
        console.log(err);
    });
    this.editDzialy=false;
    this.deactivated=true;
  }

  closeEditDzialy():void {
    this.editDzialy=false;
  }

  searchDzialy(event: any){
    if (event.target.value) {
      let text = event.target.value.toLowerCase();
      let filtered = this.dzialy.map(i => { return i.nazwa + ' ' + i.kierownik});
      filtered.filter((i, index) => {
        if(i.toLowerCase().includes(text)) 
          this.dzialy[index].display = true;
        else
          this.dzialy[index].display = false;
      });
    }
    else
      this.dzialy.forEach( i => i.display = true);
  }

}
