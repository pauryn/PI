export class WykSprzet{
    constructor(
        public id: number,
        public sprzet: string,
        public dzial: string,
        public dataRozp: Date,
        public dataZak: Date,
        public zdjecie: string,
        public display: boolean
    ){}
}