const express =  require('express');
const controller = require('../controller/sprzet.controller')

const sprzetRouter = express.Router();

sprzetRouter.get('/sprzet/all', controller.getAllSprzet);
sprzetRouter.post('/sprzet/add', controller.addSprzet);
sprzetRouter.put('/sprzet/edit/:id', controller.editSprzet);
sprzetRouter.put('/sprzet/deactivate/:id',controller.deactivateSprzet);
sprzetRouter.get('/sprzet/one/:id',controller.getOneSprzet);


module.exports = sprzetRouter;