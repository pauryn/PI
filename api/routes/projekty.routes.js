const express =  require('express');
const controller = require('../controller/projekty.controller')

const projektyRouter = express.Router();
projektyRouter.get('/projekty/all', controller.getAllProjekty);
projektyRouter.post('/projekty/add', controller.addProjekty);
projektyRouter.put('/projekty/edit/:id', controller.editProjekty);
projektyRouter.put('/projekty/deactivate/:id',controller.deactivateProjekty);
projektyRouter.get('/projekty/pracownicy',controller.getPracownicyDropdown);
projektyRouter.get('/projekty/one/:id',controller.getOneProjekty);

projektyRouter.get('/pracownicyprojekty/all/:id', controller.getAllPracownicyProjekty);
projektyRouter.get('/pracownicyprojekty/check/:id/:id2', controller.checkPracownicyProjekty);
projektyRouter.post('/pracownicyprojekty/add/:id/:id2', controller.addPracownicyProjekty);
projektyRouter.put('/pracownicyprojekty/deactivate/:id',controller.deactivatePracownicyProjekty);

projektyRouter.get('/wyksprzetu/all/:id', controller.getAllWykSprzetu);
projektyRouter.post('/wyksprzetu/add/:id', controller.addWykSprzetu);
projektyRouter.put('/wyksprzetu/edit/:id', controller.editWykSprzetu);
projektyRouter.put('/wyksprzetu/deactivate/:id',controller.deactivateWykSprzetu);
projektyRouter.get('/wyksprzetu/sprzet',controller.getSprzetDropdown);
projektyRouter.get('/wyksprzetu/dzialy',controller.getDzialytDropdown);
projektyRouter.get('/wyksprzetu/one/:id', controller.getOneWykSprzetu);

projektyRouter.get('/umowyprojekty/all/:id', controller.getAllUmowyProjekty);
projektyRouter.get('/umowyprojekty/check/:id/:id2', controller.checkUmowyProjekty);
projektyRouter.post('/umowyprojekty/add/:id/:um', controller.addUmowyProjekty);
projektyRouter.put('/umowyprojekty/deactivate/:id',controller.deactivateUmowyProjekty);
projektyRouter.get('/umowyprojekty/umowy',controller.getUmowyDropdown);

module.exports = projektyRouter;