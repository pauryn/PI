const express =  require('express');
const controller = require('../controller/login.controller')

const loginRouter = express.Router();

loginRouter.put('/login', controller.getLogin);

module.exports = loginRouter;