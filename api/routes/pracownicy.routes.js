const express =  require('express');
const controller = require('../controller/pracownicy.controller')

const pracownicyRouter = express.Router();

pracownicyRouter.get('/pracownicy/all', controller.getAllPracownicy);
pracownicyRouter.post('/pracownicy/add', controller.addPracownicy);
pracownicyRouter.put('/pracownicy/edit/:id', controller.editPracownicy);
pracownicyRouter.put('/pracownicy/deactivate/:id',controller.deactivatePracownicy);
pracownicyRouter.get('/pracownicy/one/:id',controller.getOnePracownicy);


module.exports = pracownicyRouter;