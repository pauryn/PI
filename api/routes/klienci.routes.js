const express =  require('express');
const controller = require('../controller/klienci.controller')

const klienciRouter = express.Router();

klienciRouter.get('/klienci/all', controller.getAllKlienci);
klienciRouter.post('/klienci/add', controller.addKlienci);
klienciRouter.put('/klienci/edit/:id', controller.editKlienci);
klienciRouter.put('/klienci/deactivate/:id',controller.deactivateKlienci);
klienciRouter.get('/klienci/one/:id',controller.getOneKlienci);

klienciRouter.get('/umowy/all/:id', controller.getAllUmowy);
klienciRouter.post('/umowy/add/:id', controller.addUmowy);
klienciRouter.put('/umowy/edit/:id', controller.editUmowy)
klienciRouter.get('/umowy/one/:id',controller.getOneUmowy);
klienciRouter.put('/umowy/deactivate/:id',controller.deactivateUmowy);
klienciRouter.get('/umowy/klienci',controller.getKlienciDropdown);

module.exports = klienciRouter;