const express =  require('express');
const controller = require('../controller/dzialy.controller')

const dzialyRouter = express.Router();

dzialyRouter.get('/dzialy/all', controller.getAllDzialy);
dzialyRouter.post('/dzialy/add', controller.addDzialy);
dzialyRouter.put('/dzialy/edit/:id', controller.editDzialy);
dzialyRouter.put('/dzialy/deactivate/:id',controller.deactivateDzialy);
dzialyRouter.get('/dzialy/one/:id',controller.getOneDzialy);


module.exports = dzialyRouter;