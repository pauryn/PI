const {poolPromise } = require('../database/db.js')
const fs = require('fs');
var rawdata = fs.readFileSync('./query/klienci.queries.json');
var queries = JSON.parse(rawdata);



class KlienciController {

    async getAllKlienci(req , res){
        try {
            const pool = await poolPromise;
            const result = await pool.request()
            .query(queries.getAllKlienci);
            res.json(result.recordset);
        } catch (error) {
            res.status(500)
            res.send(error.message)
        }
    }

    async addKlienci(req , res){
        try {
            const pool = await poolPromise;
            const result = await pool.request()
            .input('firma', req.body.firma)
            .input('nip', req.body.nip)
            .input('adres', req.body.adres)
            .input('tel', req.body.tel)
            .query(queries.addKlienci);
            res.json(result.recordset);
        } catch (error) {
            res.status(500)
            res.send(error.message)
        }
    }

    async editKlienci(req , res){
        try {
            const pool = await poolPromise;
            const result = await pool.request()
            .input('firma', req.body.firma)
            .input('nip', req.body.nip)
            .input('adres', req.body.adres)
            .input('tel', req.body.tel)
            .input('id', req.params.id)
            .query(queries.editKlienci);
            res.json(result.recordset);
        } catch (error) {
            res.status(500)
            res.send(error.message)
        }
    }

    async deactivateKlienci(req , res){
        try {
            const pool = await poolPromise;
            const result = await pool.request()
            .input('id', req.params.id)
            .query(queries.deactivateKlienci);
            res.json(result.recordset);
        } catch (error) {
            res.status(500)
            res.send(error.message)
        }
    }

    async getOneKlienci(req , res){
        try {
            const pool = await poolPromise;
            const result = await pool.request()
            .input('id', req.params.id)
            .query(queries.getOneKlienci);
            res.json(result.recordset);
        } catch (error) {
            res.status(500)
            res.send(error.message)
        }
    }

    async getAllUmowy(req , res){
        try {
            const pool = await poolPromise
            const result = await pool.request()
            .input('id', req.params.id)
            .query(queries.getAllUmowy)
            res.json(result.recordset)
        } catch (error) {
            res.status(500)
            res.send(error.message)
        }
    }

      
    async addUmowy(req , res){
        
        let photo = Buffer.from(req.body.zdj, 'base64');
        try {
            const pool = await poolPromise;
            const result = await pool.request()
            .input('id', req.params.id)
            .input('cena', req.body.cena)
            .input('zaw', req.body.zaw)
            .input('zak', req.body.zak)
            .input('zdj', photo)
            .query(queries.addUmowy);
            res.json(result.recordset);
        } catch (error) {
            res.send(error.message)
        }
    }

    async getOneUmowy(req , res){
        try {
            const pool = await poolPromise;
            const result = await pool.request()
            .input('id', req.params.id)
            .query(queries.getOneUmowy);
            res.json(result.recordset);
        } catch (error) {
            res.status(500)
            res.send(error.message)
        }
    }

    
    async editUmowy(req , res){
        if(req.body.zdj != '') {
            let photo = Buffer.from(req.body.zdj, 'base64');
            try {
                const pool = await poolPromise;
                const result = await pool.request()
                    .input('kl', req.body.kl)
                    .input('cena', req.body.cena)
                    .input('zaw', req.body.zaw)
                    .input('zak', req.body.zak)
                    .input('zdj', photo)
                    .input('id', req.params.id)
                    .query(queries.editUmowyPhoto);
                res.json(result.recordset);
            } catch (error) {
                res.status(500)
                res.send(error.message)
            }
        } else {
            try {
                const pool = await poolPromise;
                const result = await pool.request()
                    .input('kl', req.body.kl)
                    .input('cena', req.body.cena)
                    .input('zaw', req.body.zaw)
                    .input('zak', req.body.zak)
                    .input('id', req.params.id)
                    .query(queries.editUmowy);
                    res.json(result.recordset);
                } catch (error) {
                    res.status(500)
                    res.send(error.message)
                }
        }
        
    }

    async deactivateUmowy(req , res){
        try {
            const pool = await poolPromise;
            const result = await pool.request()
            .input('id', req.params.id)
            .query(queries.deactivateUmowy);
            res.json(result.recordset);
        } catch (error) {
            res.status(500)
            res.send(error.message)
        }
    }

    
    async getKlienciDropdown(req , res){
        try {
            const pool = await poolPromise
            const result = await pool.request()
            .query(queries.getKlienciDropdown)
            res.json(result.recordset)
        } catch (error) {
            res.status(500)
            res.send(error.message)
        }
    }

}
const controller = new KlienciController()
module.exports = controller;