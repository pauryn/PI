const {poolPromise } = require('../database/db.js')
const fs = require('fs');
var rawdata = fs.readFileSync('./query/login.queries.json');
var queries = JSON.parse(rawdata);
var crypto = require('crypto');

class LoginController {

    async getLogin(req , res){
        try {
            var hash = crypto.createHash('sha512');
            var datas = hash.update(req.body.haslo);
            var gen_hash= datas.digest('hex');
            const pool = await poolPromise;
            const result = await pool.request()
            .input('login', req.body.login)
            .query(queries.getPassword)
            if(result.recordset.length>0) {
                var r = Buffer.from(result.recordset[0].PR_Haslo).toString('hex');
                if (r==gen_hash) {
                    const pool2 = await poolPromise;
                    const result2 = await pool.request()
                    .input('id', result.recordset[0].PR_Id)
                    .query(queries.getUser)
                    res.send({"result": true,
                            "imie": result2.recordset[0].PR_Imie,
                            "zdjecie": result2.recordset[0].PR_Zdjecie,
                            "nazwisko": result2.recordset[0].PR_Nazwisko,
                            "dzial": result2.recordset[0].DZ_Nazwa,
                            "id": result.recordset[0].PR_Id
                            });
                }
                   
                else
                    res.send({"result": false});
            }
            else
                res.send({"result": false});
            
        } catch (error) {
            res.status(500)
            res.send(error.message)
        }
    }
}

const controller = new LoginController();
module.exports = controller;