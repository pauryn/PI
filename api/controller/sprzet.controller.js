const {poolPromise } = require('../database/db.js')
const fs = require('fs');
var rawdata = fs.readFileSync('./query/sprzet.queries.json');
var queries = JSON.parse(rawdata);



class SprzetController {

    async getAllSprzet(req , res){
        try {
            const pool = await poolPromise;
            const result = await pool.request()
            .query(queries.getAllSprzet);
            res.json(result.recordset);
        } catch (error) {
            res.status(500)
            res.send(error.message)
        }
    }

    async addSprzet(req , res){
        let photo = Buffer.from(req.body.zdj, 'base64');
        if(req.body.wyc == "") req.body.wyc=null;
        try {
            const pool = await poolPromise;
            const result = await pool.request()
            .input('dz', req.body.dz)
            .input('nazwa', req.body.nazwa)
            .input('wpr', req.body.wpr)
            .input('wyc', req.body.wyc)
            .input('zdj', photo)
            .query(queries.addSprzet);
            res.json(result.recordset);
        } catch (error) {
            res.status(500)
            res.send(error.message)
        }
    }

    async editSprzet(req , res){
        if(req.body.wyc == "") req.body.wyc=null;
        if(req.body.zdj != '') {
            let photo = Buffer.from(req.body.zdj, 'base64');
            try {
                const pool = await poolPromise;
                const result = await pool.request()
                    .input('id', req.params.id)
                    .input('dz', req.body.dz)
                    .input('nazwa', req.body.nazwa)
                    .input('wpr', req.body.wpr)
                    .input('wyc', req.body.wyc)
                    .input('zdj', photo)
                    .query(queries.editSprzetPhoto);
                res.json(result.recordset);
            } catch (error) {
                res.status(500)
                res.send(error.message)
            }
        } else {
            try {
                const pool = await poolPromise;
                const result = await pool.request()
                    .input('id', req.params.id)
                    .input('dz', req.body.dz)
                    .input('nazwa', req.body.nazwa)
                    .input('wpr', req.body.wpr)
                    .input('wyc', req.body.wyc)
                    .query(queries.editSprzet);
                    res.json(result.recordset);
                } catch (error) {
                    res.status(500)
                    res.send(error.message)
                }
        }
    }

    async deactivateSprzet(req , res){
        try {
            const pool = await poolPromise;
            const result = await pool.request()
            .input('id', req.params.id)
            .query(queries.deactivateSprzet);
            res.json(result.recordset);
        } catch (error) {
            res.status(500)
            res.send(error.message)
        }
    }

    async getOneSprzet(req , res){
        try {
            const pool = await poolPromise;
            const result = await pool.request()
            .input('id', req.params.id)
            .query(queries.getOneSprzet);
            res.json(result.recordset);
        } catch (error) {
            res.status(500)
            res.send(error.message)
        }
    }

}
const controller = new SprzetController()
module.exports = controller;