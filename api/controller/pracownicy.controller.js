const {poolPromise } = require('../database/db.js')
const fs = require('fs');
var rawdata = fs.readFileSync('./query/pracownicy.queries.json');
var queries = JSON.parse(rawdata);



class PracownicyController {

    async getAllPracownicy(req , res){
        try {
            const pool = await poolPromise;
            const result = await pool.request()
            .query(queries.getAllPracownicy);
            res.json(result.recordset);
        } catch (error) {
            res.status(500)
            res.send(error.message)
        }
    }

    async addPracownicy(req , res){
        if(req.body.zwol == "") req.body.zwol=null;
        let photo = Buffer.from(req.body.zdj, 'base64');
        try {
            const pool = await poolPromise;
            const result = await pool.request()
            .input('dz', req.body.dz)
            .input('imie', req.body.imie)
            .input('nazw', req.body.nazw)
            .input('ur', req.body.ur)
            .input('adres', req.body.adres)
            .input('zatr', req.body.zatr)
            .input('zwol', req.body.zwol)
            .input('pesel', req.body.pesel)
            .input('tel', req.body.tel)
            .input('zdj', photo)
            .input('login', req.body.imie.concat(req.body.nazw))
            .input('haslo', 'password'.concat(req.body.imie))
            .query(queries.addPracownicy);
            res.json(result.recordset);
        } catch (error) {
            res.status(500)
            res.send(error.message)
        }
    }

    async editPracownicy(req , res){
        if(req.body.zwol == "") req.body.zwol=null;
        if(req.body.zdj != '') {
            let photo = Buffer.from(req.body.zdj, 'base64');
            try {
                const pool = await poolPromise;
                const result = await pool.request()
                    .input('id', req.params.id)
                    .input('dz', req.body.dz)
                    .input('imie', req.body.imie)
                    .input('nazw', req.body.nazw)
                    .input('ur', req.body.ur)
                    .input('adres', req.body.adres)
                    .input('zatr', req.body.zatr)
                    .input('zwol', req.body.zwol)
                    .input('pesel', req.body.pesel)
                    .input('tel', req.body.tel)
                    .input('zdj', photo)
                    .query(queries.editPracownicyPhoto);
                res.json(result.recordset);
            } catch (error) {
                res.status(500)
                res.send(error.message)
            }
        } else {
            try {
                const pool = await poolPromise;
                const result = await pool.request()
                    .input('id', req.params.id)
                    .input('dz', req.body.dz)
                    .input('imie', req.body.imie)
                    .input('nazw', req.body.nazw)
                    .input('ur', req.body.ur)
                    .input('adres', req.body.adres)
                    .input('zatr', req.body.zatr)
                    .input('zwol', req.body.zwol)
                    .input('pesel', req.body.pesel)
                    .input('tel', req.body.tel)
                    .query(queries.editPracownicy);
                    res.json(result.recordset);
                } catch (error) {
                    res.status(500)
                    res.send(error.message)
                }
        }
    }

    async deactivatePracownicy(req , res){
        try {
            const pool = await poolPromise;
            const result = await pool.request()
            .input('id', req.params.id)
            .query(queries.deactivatePracownicy);
            res.json(result.recordset);
        } catch (error) {
            res.status(500)
            res.send(error.message)
        }
    }

    async getOnePracownicy(req , res){
        try {
            const pool = await poolPromise;
            const result = await pool.request()
            .input('id', req.params.id)
            .query(queries.getOnePracownicy);
            res.json(result.recordset);
        } catch (error) {
            res.status(500)
            res.send(error.message)
        }
    }

}
const controller = new PracownicyController()
module.exports = controller;