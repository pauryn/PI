const {poolPromise } = require('../database/db.js')
const fs = require('fs');
var rawdata = fs.readFileSync('./query/projekty.queries.json');
var queries = JSON.parse(rawdata);

class ProjektyController {

    async getAllProjekty(req , res){
        try {
            const pool = await poolPromise;
            const result = await pool.request()
            .query(queries.getAllProjekty);
            res.json(result.recordset);
        } catch (error) {
            res.status(500)
            res.send(error.message)
        }
    }

    async addProjekty(req , res){
        try {
            const pool = await poolPromise;
            const result = await pool.request()
            .input('pr', req.body.pr)
            .input('nazwa', req.body.nazwa)
            .input('deadline', req.body.deadline)
            .input('cena', req.body.cena)
            .input('rozp', req.body.rozp)
            .input('zak', req.body.zak)
            .query(queries.addProjekty);
            res.json(result.recordset);
        } catch (error) {
            res.status(500)
            res.send(error.message)
        }
    }

    async editProjekty(req , res){
        try {
            const pool = await poolPromise;
            const result = await pool.request()
            .input('pr', req.body.pr)
            .input('nazwa', req.body.nazwa)
            .input('deadline', req.body.deadline)
            .input('cena', req.body.cena)
            .input('rozp', req.body.rozp)
            .input('zak', req.body.zak)
            .input('id', req.params.id)
            .query(queries.editProjekty);
            res.json(result.recordset);
        } catch (error) {
            res.status(500)
            res.send(error.message)
        }
    }

    async deactivateProjekty(req , res){
        try {
            const pool = await poolPromise;
            const result = await pool.request()
            .input('id', req.params.id)
            .query(queries.deactivateProjekty);
            res.json(result.recordset);
        } catch (error) {
            res.status(500)
            res.send(error.message)
        }
    }

    async getPracownicyDropdown(req , res){
        try {
            const pool = await poolPromise
            const result = await pool.request()
            .query(queries.getPracownicyDropdown)
            res.json(result.recordset)
        } catch (error) {
            res.status(500)
            res.send(error.message)
        }
    }

    async getOneProjekty(req , res){
        try {
            const pool = await poolPromise;
            const result = await pool.request()
            .input('id', req.params.id)
            .query(queries.getOneProjekty);
            res.json(result.recordset);
        } catch (error) {
            res.status(500)
            res.send(error.message)
        }
    }

    async getAllPracownicyProjekty(req , res){
        try {
            const pool = await poolPromise
            const result = await pool.request()
            .input('id', req.params.id)
            .query(queries.getAllPracownicyProjekty)
            res.json(result.recordset)
        } catch (error) {
            res.status(500)
            res.send(error.message)
        }
    }

    async checkPracownicyProjekty(req , res){
        try {
            const pool = await poolPromise
            const result = await pool.request()
            .input('id', req.params.id)
            .input('id2', req.params.id2)
            .query(queries.checkPracownicyProjekty)
            res.json(result.recordset)
        } catch (error) {
            res.status(500)
            res.send(error.message)
        }
    }

    async addPracownicyProjekty(req , res){
        try {
            const pool = await poolPromise;
            const result = await pool.request()
            .input('id2', req.params.id2)
            .input('id', req.params.id)
            .query(queries.addPracownicyProjekty);
            res.json(result.recordset);
        } catch (error) {
            res.status(500)
            res.send(error.message)
        }
    }

    async deactivatePracownicyProjekty(req , res){
        try {
            const pool = await poolPromise;
            const result = await pool.request()
            .input('id', req.params.id)
            .query(queries.deactivatePracownicyProjekty);
            res.json(result.recordset);
        } catch (error) {
            res.status(500)
            res.send(error.message)
        }
    }

    async getAllWykSprzetu(req , res){
        try {
            const pool = await poolPromise
            const result = await pool.request()
            .input('id', req.params.id)
            .query(queries.getAllWykSprzetu)
            res.json(result.recordset)
        } catch (error) {
            res.status(500)
            res.send(error.message)
        }
    }
    
    async addWykSprzetu(req , res){
        try {
            const pool = await poolPromise;
            const result = await pool.request()
            .input('dz', req.body.dz)
            .input('sp', req.body.sp)
            .input('proj', req.body.proj)
            .input('rozp', req.body.rozp)
            .input('zak', req.body.zak)
            .input('id', req.params.id)
            .query(queries.addWykSprzetu);
            res.json(result.recordset);
        } catch (error) {
            res.status(500)
            res.send(error.message)
        }
    }

    async getOneWykSprzetu(req , res){
        try {
            const pool = await poolPromise;
            const result = await pool.request()
            .input('id', req.params.id)
            .query(queries.getOneWykSprzetu);
            res.json(result.recordset);
        } catch (error) {
            res.status(500)
            res.send(error.message)
        }
    }

    async getSprzetDropdown(req , res){
        try {
            const pool = await poolPromise
            const result = await pool.request()
            .query(queries.getSprzetDropdown)
            res.json(result.recordset)
        } catch (error) {
            res.status(500)
            res.send(error.message)
        }
    }

    async getDzialytDropdown(req , res){
        try {
            const pool = await poolPromise
            const result = await pool.request()
            .query(queries.getDzialyDropdown)
            res.json(result.recordset)
        } catch (error) {
            res.status(500)
            res.send(error.message)
        }
    }

    async editWykSprzetu(req , res){
        try {
            const pool = await poolPromise;
            const result = await pool.request()
            .input('dz', req.body.dz)
            .input('sp', req.body.sp)
            .input('rozp', req.body.rozp)
            .input('zak', req.body.zak)
            .input('id', req.params.id)
            .query(queries.editWykSprzetu);
            res.json(result.recordset);
        } catch (error) {
            res.status(500)
            res.send(error.message)
        }
    }

    async deactivateWykSprzetu(req , res){
        try {
            const pool = await poolPromise;
            const result = await pool.request()
            .input('id', req.params.id)
            .query(queries.deactivateWykSprzetu);
            res.json(result.recordset);
        } catch (error) {
            res.status(500)
            res.send(error.message)
        }
    }

    async getAllUmowyProjekty(req , res){
        try {
            const pool = await poolPromise
            const result = await pool.request()
            .input('id', req.params.id)
            .query(queries.getAllUmowyProjekty)
            res.json(result.recordset)
        } catch (error) {
            res.status(500)
            res.send(error.message)
        }
    }

    async checkUmowyProjekty(req , res){
        try {
            const pool = await poolPromise
            const result = await pool.request()
            .input('id', req.params.id)
            .input('id2', req.params.id2)
            .query(queries.checkUmowyProjekty)
            res.json(result.recordset)
        } catch (error) {
            res.status(500)
            res.send(error.message)
        }
    }

    async addUmowyProjekty(req , res){
        try {
            const pool = await poolPromise;
            const result = await pool.request()
            .input('id', req.params.id)
            .input('um', req.params.um)
            .query(queries.addUmowyProjekty);
            res.json(result.recordset);
        } catch (error) {
            res.status(500)
            res.send(error.message)
        }
    }

    async deactivateUmowyProjekty(req , res){
        try {
            const pool = await poolPromise;
            const result = await pool.request()
            .input('id', req.params.id)
            .query(queries.deactivateUmowyProjekty);
            res.json(result.recordset);
        } catch (error) {
            res.status(500)
            res.send(error.message)
        }
    }

    async getUmowyDropdown(req , res){
        try {
            const pool = await poolPromise
            const result = await pool.request()
            .query(queries.getUmowyDropdown)
            res.json(result.recordset)
        } catch (error) {
            res.status(500)
            res.send(error.message)
        }
    }

}
const controller = new ProjektyController()
module.exports = controller;