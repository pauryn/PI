const {poolPromise } = require('../database/db.js')
const fs = require('fs');
var rawdata = fs.readFileSync('./query/dzialy.queries.json');
var queries = JSON.parse(rawdata);



class DzialyController {

    async getAllDzialy(req , res){
        try {
            const pool = await poolPromise;
            const result = await pool.request()
            .query(queries.getAllDzialy);
            res.json(result.recordset);
        } catch (error) {
            res.status(500)
            res.send(error.message)
        }
    }

    async addDzialy(req , res){
        let id;
        try {
            const pool = await poolPromise;
            const result = await pool.request()
            .input('pr', req.body.pr)
            .input('nazwa', req.body.nazwa)
            .query(queries.addDzialy);   
            try {
                const pool = await poolPromise;
                const result = await pool.request()
                    .input('pr', req.body.pr)
                    .query(queries.getAddedId);
                id=res.DZ_Id;
                try {
                const pool = await poolPromise;
                const result = await pool.request()
                    .input('pr', req.body.pr)
                    .input('id', id)
                .query(queries.editKierownik);
                res.json(result.recordset);
                } catch (error) {
                    res.status(500)
                    res.send(error.message)
                }   
            } catch (error) {
                res.status(500)
                res.send(error.message)
            }           
        } catch (error) {
            res.status(500)
            res.send(error.message)
        }  
       


    }

    async editDzialy(req , res){
       
        try {
            const pool = await poolPromise;
            const result = await pool.request()
                .input('id', req.params.id)
                .input('pr', req.body.pr)
                .input('nazwa', req.body.nazwa)
                .query(queries.editDzialy);
            }
        catch (error) {
            res.status(500)
            res.send(error.message)
        }

        try {
            const pool = await poolPromise;
            const result = await pool.request()
                .input('pr', req.body.pr)
                .input('id', req.params.id)
                .query(queries.editKierownik);
            res.json(result.recordset);
        } catch (error) {
            res.status(500)
            res.send(error.message)
        }

    
    }

    async deactivateDzialy(req , res){
        try {
            const pool = await poolPromise;
            const result = await pool.request()
            .input('id', req.params.id)
            .query(queries.deactivateDzialy);
            res.json(result.recordset);
        } catch (error) {
            res.status(500)
            res.send(error.message)
        }
    }

    async getOneDzialy(req , res){
        try {
            const pool = await poolPromise;
            const result = await pool.request()
            .input('id', req.params.id)
            .query(queries.getOneDzialy);
            res.json(result.recordset);
        } catch (error) {
            res.status(500)
            res.send(error.message)
        }
    }

}
const controller = new DzialyController()
module.exports = controller;