const express = require('express');
const cors = require('cors');
const path = require('path');
const projektyRouter = require('./routes/projekty.routes');
const loginRouter = require('./routes/login.routes');
const klienciRouter = require('./routes/klienci.routes');
const pracownicyRouter = require('./routes/pracownicy.routes');
const sprzetRouter = require('./routes/sprzet.routes');
const dzialyRouter = require('./routes/dzialy.routes');

const app = express();
  
app.use(cors());
app.use(express.urlencoded({ extended: true }));
app.use(express.json());

app.use(express.static(path.join(__dirname, 'dist')));

app.use('/api', projektyRouter);
app.use('/api', loginRouter);
app.use('/api', klienciRouter);
app.use('/api', pracownicyRouter)
app.use('/api', sprzetRouter);
app.use('/api', dzialyRouter);
  
app.get('*', (req, res) => {
    res.sendFile(path.join(__dirname, 'dist/index.html'));
  });

const port = process.env.PORT || 8000;
  
app.listen(port, () => {
    console.log('Listening on port ' + port)
})
